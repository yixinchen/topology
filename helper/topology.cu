#include <stdint.h>
#include <stdio.h>

const uint64_t ULLINT_MAX = 0xFFFFFFFFFFFFFFFF;
const int UNROLL = 8; // if changed to a different value, kernel code needs to be changed accordingly
const int MAX_BASE = 16; // maximum number of discrete feature values

int mod(int a, int b)
{
	int r = a % b;
	return r < 0 ? r + b : r;
}

// find ids of the grid inside a window
void findwindow1d(int* idx, int id, int wsize, int d)
{
	for (int i = 0; i < wsize; i++) {
		idx[i] = (id+i) % d;	// circular windows
	}
}

// find 1d indices of all pixels inside a 2d window
void findwindow(int* idx, int id, int wsize, int w, int h)
{
	int r = id % h; // find row index of top left corner of the 2d window
	int c = id / h; // find column index of top left corner of the 2d window
	int cnt = 0;
	for (int i = 0; i < wsize; i++) {
		int tmp = (r+i) % h;
		for (int j = 0; j < wsize; j++) {
			idx[cnt] = tmp + ((c+j) % w) * h;
			cnt++;
		}
	}
}

// find windows affected by a swap, return the number and indices of affected windows
int findaffected1d(int* affected, int s, int e, int wsize, int d) 
{
	for (int i = 0; i < wsize; i++) {
		int r = (s-i) % d;
		affected[i] = r < 0 ? r + d : r;
		r = (e-i) % d;
		affected[i+wsize] = r < 0 ? r + d : r;
	}
	int cnt = 0;
	bool flag;
	for (int i = 0; i < wsize; i++) {
		flag = true;
		int tmp = i + wsize;
		for (int j = 0; j < wsize; j++) {
			if (affected[tmp] == affected[j]) {
				flag = false;
				break;
			}
		}
		if (flag) {
			affected[wsize + cnt] = affected[tmp];
			cnt++;
		}
	}
	return cnt + wsize;
}

// find 1d indices of windows affected by a swap, return the number and indices of affected windows
int findaffected(int* affected, int s, int e, int wsize, int w, int h)
{
	int rs = s % h; // row index of pixel s
	int cs = s / h; // column index of pixel s
	int re = e % h; // row index of pixel e
	int ce = e / h; // column index of pixel e
	int tmp, rrs, ccs, rre, cce;
	int cnt = 0;
	int wsize2 = wsize * wsize;
	for (int i = 0; i < wsize; i++) {
		tmp = (rs - i) % h;
		rrs = tmp < 0 ? tmp + h : tmp;
		tmp = (re - i) % h;
		rre = tmp < 0 ? tmp + h : tmp;
		for (int j = 0; j < wsize; j++) {
			tmp = (cs - j) % w;
			ccs = tmp < 0 ? tmp + w : tmp;
			affected[cnt] = rrs + ccs * h;
			tmp = (ce - j) % w;
			cce = tmp < 0 ? tmp + w : tmp;
			affected[cnt + wsize2] = rre + cce * h;
			cnt++;
		}
	}
	cnt = 0;
	bool flag;
	for (int i = 0; i < wsize2; i++) {
		flag = true;
		for (int j = 0; j < wsize2; j++) {
			if (affected[i + wsize2] == affected[j]) {
				flag = false;
				break;
			}
		}
		if (flag) {
			affected[wsize2 + cnt] = affected[wsize2 + i];
			cnt++;
		}
	}
	return cnt + wsize2;
}

// find 1d indices of windows affected by a swap of two blocks, return the number and indices of affected windows
int findaffectedblock(int* affected, int s, int e, int blocksize, int wsize, int w, int h)
{
	int bh = h / blocksize;
	int rs = s % bh;
	int cs = s / bh;
	int re = e % bh;
	int ce = e / bh;
	int rss = rs * blocksize + blocksize - 1;
	int css = cs * blocksize + blocksize - 1;
	int ree = re * blocksize + blocksize - 1;
	int cee = ce * blocksize + blocksize - 1;
	int S = css * h + rss;
	int E = cee * h + ree;
	return findaffected(affected, S, E, wsize+blocksize-1, w, h);
}

// shuffle blocks according to bid
void blockshuffle(int* idx, int bsize, int h, int* bid, int len)
{
	int rs, cs, re, ce, rss, css, ree, cee, S, E;
	int bh = h / bsize;
	
	for (int i = 0; i < len; i++) {
		rs = i % bh;
		cs = i / bh;
		re = bid[i] % bh;
		ce = bid[i] / bh;
		rss = rs * bsize;
		css = cs * bsize;
		ree = re * bsize;
		cee = ce * bsize;
		for (int j = 0; j < bsize; j++) {
			int tmp1 = (css + j) * h + rss;
			int tmp2 = (cee + j) * h + ree;
			for (int k = 0; k < bsize; k++) {
				S = tmp1 + k;
				E = tmp2 + k;
				idx[S] = E;
			}
		}
	}
}

// update pixel indices after swapping two blocks s and e, idx_out and idx_in can point to the same address
void blockswap(int* idx_out, int* idx_in, int s, int e, int bsize, int h)
{
	int bh = h / bsize;
	int rs = s % bh;
	int cs = s / bh;
	int re = e % bh;
	int ce = e / bh;
	int rss = rs * bsize;
	int css = cs * bsize;
	int ree = re * bsize;
	int cee = ce * bsize;
	int tmp, tmp1, tmp2, S, E;
	
	for (int i = 0; i < bsize; i++) {
		tmp1 = (css + i) * h + rss;
		tmp2 = (cee + i) * h + ree;
		for (int j = 0; j< bsize; j++) {
			S = tmp1 + j;
			E = tmp2 + j;
			tmp = idx_in[S]; // idx_out and idx_in can point to the same address
			idx_out[S] = idx_in[E];
			idx_out[E] = tmp;
		}
	}
}

void multinomialent(double* ent, int* data, int* col_id, int* hist, int batch_len, int n, int N, int wsize, int base, double* table)
{
	int* tmpidx;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmpidx = col_id + i * wsize;
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < base; k++) {
				hist[k] = 0;
			}
			for (int k = 0; k < wsize; k++) {
				hist[*(data + *(tmpidx +k)*N + j)] += 1; // column major
			}
			double tmp = table[hist[0]];
			for (int k = 1; k < base; k++) {
				tmp += table[hist[k]];
			}
			ent[i] += tmp;
		}
	}
}

__global__ void mulnom(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, int wsize, int base, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;
	int hist[MAX_BASE];

	if (idx + 7*blockDim.x < N) {
		int tmp = wsize * blockIdx.y;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx]] += 1;
		}
		gtdata[idx] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + blockDim.x]] += 1;
		}
		gtdata[idx+blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x]] += 1;
		}
		gtdata[idx+2*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+2*blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x]] += 1;
		}
		gtdata[idx+3*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+3*blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x]] += 1;
		}
		gtdata[idx+4*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+4*blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x]] += 1;
		}
		gtdata[idx+5*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+5*blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x]] += 1;
		}
		gtdata[idx+6*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+6*blockDim.x] += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x]] += 1;
		}
		gtdata[idx+7*blockDim.x] = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			gtdata[idx+7*blockDim.x] += g_table[hist[i]];
		}
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 3, base = 3
void multinomialent3_3(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[3];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *3;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]];
		}
	}
}

__global__ void mulnom3_3(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		int hist[3];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 4, base = 3
void multinomialent4_3(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[3];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *4;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 3)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]];
		}
	}
}

__global__ void mulnom4_3(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		int hist[3];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 5, base = 3
void multinomialent5_3(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[3];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *5;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 3)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 4)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]];
		}
	}
}

__global__ void mulnom5_3(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		int hist[3];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		hist[g_idata[i4]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		hist[g_idata[i4+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		hist[g_idata[i4+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		hist[g_idata[i4+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		hist[g_idata[i4+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		hist[g_idata[i4+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		hist[g_idata[i4+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		hist[g_idata[i4+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 3, base = 4
void multinomialent3_4(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[4];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *3;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[3] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]] + table[hist[3]];
		}
	}
}

__global__ void mulnom3_4(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		int hist[4];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 4, base = 4
void multinomialent4_4(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[4];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *4;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[3] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 3)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]] + table[hist[3]];
		}
	}
}

__global__ void mulnom4_4(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		int hist[4];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 5, base = 4
void multinomialent5_4(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[4];
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i *5;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[3] = 0;
			hist[*(data + *(tmp)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 1)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 2)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 3)*N + j)] += 1; // column major
			hist[*(data + *(tmp + 4)*N + j)] += 1; // column major
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]] + table[hist[3]];
		}
	}
}

__global__ void mulnom5_4(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		int hist[4];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		hist[g_idata[i4]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		hist[g_idata[i4+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		hist[g_idata[i4+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		hist[g_idata[i4+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		hist[g_idata[i4+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		hist[g_idata[i4+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		hist[g_idata[i4+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		hist[g_idata[i4+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 3, base = 5
void multinomialent3_5(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[5];
	int tmp, tmp1, tmp2;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*3] * N;
		tmp1 = col_id[i*3+1] * N;
		tmp2 = col_id[i*3+2] * N;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[3] = 0;
			hist[4] = 0;
			hist[data[tmp+j]] += 1;
			hist[data[tmp1+j]] += 1;
			hist[data[tmp2+j]] += 1;
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]] + table[hist[3]] + table[hist[4]];
		}
	}
}

__global__ void mulnom3_5(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		int hist[5];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 4, base = 5
void multinomialent4_5(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int hist[5];
	int tmp, tmp1, tmp2, tmp3;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*4] * N;
		tmp1 = col_id[i*4+1] * N;
		tmp2 = col_id[i*4+2] * N;
		tmp3 = col_id[i*4+3] * N;
		for (int j = 0; j < n; j++) {
			hist[0] = 0;
			hist[1] = 0;
			hist[2] = 0;
			hist[3] = 0;
			hist[4] = 0;
			hist[data[tmp+j]] += 1;
			hist[data[tmp1+j]] += 1;
			hist[data[tmp2+j]] += 1;
			hist[data[tmp3+j]] += 1;
			ent[i] += table[hist[0]] + table[hist[1]] + table[hist[2]] + table[hist[3]] + table[hist[4]];
		}
	}
}

__global__ void mulnom4_5(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		int hist[5];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 5, base = 5
__global__ void mulnom5_5(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		int hist[5];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		hist[g_idata[i4]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		hist[g_idata[i4+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		hist[g_idata[i4+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		hist[g_idata[i4+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		hist[g_idata[i4+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		hist[g_idata[i4+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		hist[g_idata[i4+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		hist[g_idata[i4+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 3, base = 6
__global__ void mulnom3_6(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		int hist[6];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 4, base = 6
__global__ void mulnom4_6(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		int hist[6];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 5, base = 6
__global__ void mulnom5_6(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		int hist[6];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		hist[g_idata[i4]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		hist[g_idata[i4+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		hist[g_idata[i4+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		hist[g_idata[i4+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		hist[g_idata[i4+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		hist[g_idata[i4+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		hist[g_idata[i4+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		hist[g_idata[i4+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 3, base = 7
__global__ void mulnom3_7(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		int hist[7];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] \
		+ g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 4, base = 7
__global__ void mulnom4_7(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		int hist[7];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] \
		+ g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// wsize = 5, base = 7
__global__ void mulnom5_7(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		int hist[7];
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0]] += 1;
		hist[g_idata[i1]] += 1;
		hist[g_idata[i2]] += 1;
		hist[g_idata[i3]] += 1;
		hist[g_idata[i4]] += 1;
		gtdata[idx] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + g_table[hist[3]] \
		+ g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+blockDim.x]] += 1;
		hist[g_idata[i1+blockDim.x]] += 1;
		hist[g_idata[i2+blockDim.x]] += 1;
		hist[g_idata[i3+blockDim.x]] += 1;
		hist[g_idata[i4+blockDim.x]] += 1;
		gtdata[idx+blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+2*blockDim.x]] += 1;
		hist[g_idata[i1+2*blockDim.x]] += 1;
		hist[g_idata[i2+2*blockDim.x]] += 1;
		hist[g_idata[i3+2*blockDim.x]] += 1;
		hist[g_idata[i4+2*blockDim.x]] += 1;
		gtdata[idx+2*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+3*blockDim.x]] += 1;
		hist[g_idata[i1+3*blockDim.x]] += 1;
		hist[g_idata[i2+3*blockDim.x]] += 1;
		hist[g_idata[i3+3*blockDim.x]] += 1;
		hist[g_idata[i4+3*blockDim.x]] += 1;
		gtdata[idx+3*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+4*blockDim.x]] += 1;
		hist[g_idata[i1+4*blockDim.x]] += 1;
		hist[g_idata[i2+4*blockDim.x]] += 1;
		hist[g_idata[i3+4*blockDim.x]] += 1;
		hist[g_idata[i4+4*blockDim.x]] += 1;
		gtdata[idx+4*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+5*blockDim.x]] += 1;
		hist[g_idata[i1+5*blockDim.x]] += 1;
		hist[g_idata[i2+5*blockDim.x]] += 1;
		hist[g_idata[i3+5*blockDim.x]] += 1;
		hist[g_idata[i4+5*blockDim.x]] += 1;
		gtdata[idx+5*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+6*blockDim.x]] += 1;
		hist[g_idata[i1+6*blockDim.x]] += 1;
		hist[g_idata[i2+6*blockDim.x]] += 1;
		hist[g_idata[i3+6*blockDim.x]] += 1;
		hist[g_idata[i4+6*blockDim.x]] += 1;
		gtdata[idx+6*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		hist[0] = 0;
		hist[1] = 0;
		hist[2] = 0;
		hist[3] = 0;
		hist[4] = 0;
		hist[5] = 0;
		hist[6] = 0;
		hist[g_idata[i0+7*blockDim.x]] += 1;
		hist[g_idata[i1+7*blockDim.x]] += 1;
		hist[g_idata[i2+7*blockDim.x]] += 1;
		hist[g_idata[i3+7*blockDim.x]] += 1;
		hist[g_idata[i4+7*blockDim.x]] += 1;
		gtdata[idx+7*blockDim.x] = g_table[hist[0]] + g_table[hist[1]] + g_table[hist[2]] + \
		g_table[hist[3]] + g_table[hist[4]] + g_table[hist[5]] + g_table[hist[6]];
		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

void binogini(double* ent, int* data, int* col_id, int batch_len, int n, int N, int wsize, double* table)
{
	int accu;
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i * wsize;
		for (int j = 0; j < n; j++) {
			accu = 0;
			for (int k = 0; k < wsize; k++) {
				accu += data[*(tmp +k)*N + j];
			}
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, int wsize, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = wsize * blockIdx.y;
		//double accu;
		unsigned int accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx];
		}
		gtdata[idx] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + blockDim.x];
		}
		gtdata[idx+blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x];
		}
		gtdata[idx+2*blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x];
		}
		gtdata[idx+3*blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x];
		}
		gtdata[idx+4*blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x];
		}
		gtdata[idx+5*blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x];
		}
		gtdata[idx+6*blockDim.x] = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x];
		}
		gtdata[idx+7*blockDim.x] = table[accu];

		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}


// binary entropy, wsize = 3
void binogini3_2(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int accu;
	int tmp, tmp1, tmp2;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*3] * N;
		tmp1 = col_id[i*3+1] * N;
		tmp2 = col_id[i*3+2] * N;
		for (int j = 0; j < n; j++) {
			accu = data[tmp+j] + data[tmp1+j] + data[tmp2+j]; // column major
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini3_2(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 3 * blockIdx.y;
		unsigned int accu;
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		accu = g_idata[i0] + g_idata[i1] + g_idata[i2];
		gtdata[idx] = table[accu];
		accu = g_idata[i0+blockDim.x] + g_idata[i1+blockDim.x] + g_idata[i2+blockDim.x];
		gtdata[idx+blockDim.x] = table[accu];
		accu = g_idata[i0+2*blockDim.x] + g_idata[i1+2*blockDim.x] + g_idata[i2+2*blockDim.x];
		gtdata[idx+2*blockDim.x] = table[accu];
		accu = g_idata[i0+3*blockDim.x] + g_idata[i1+3*blockDim.x] + g_idata[i2+3*blockDim.x];
		gtdata[idx+3*blockDim.x] = table[accu];
		accu = g_idata[i0+4*blockDim.x] + g_idata[i1+4*blockDim.x] + g_idata[i2+4*blockDim.x];
		gtdata[idx+4*blockDim.x] = table[accu];
		accu = g_idata[i0+5*blockDim.x] + g_idata[i1+5*blockDim.x] + g_idata[i2+5*blockDim.x];
		gtdata[idx+5*blockDim.x] = table[accu];
		accu = g_idata[i0+6*blockDim.x] + g_idata[i1+6*blockDim.x] + g_idata[i2+6*blockDim.x];
		gtdata[idx+6*blockDim.x] = table[accu];
		accu = g_idata[i0+7*blockDim.x] + g_idata[i1+7*blockDim.x] + g_idata[i2+7*blockDim.x];
		gtdata[idx+7*blockDim.x] = table[accu];

		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// binary entropy, wsize = 4
void binogini4_2(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int accu;
	int tmp, tmp1, tmp2, tmp3;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*4] * N;
		tmp1 = col_id[i*4+1] * N;
		tmp2 = col_id[i*4+2] * N;
		tmp3 = col_id[i*4+3] * N;
		for (int j = 0; j < n; j++) {
			accu = data[tmp+j] + data[tmp1+j] + data[tmp2+j] + data[tmp3+j]; // column major
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini4_2(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 4 * blockIdx.y;
		unsigned int accu;
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		accu = g_idata[i0] + g_idata[i1] + g_idata[i2] + g_idata[i3];
		gtdata[idx] = table[accu];
		accu = g_idata[i0+blockDim.x] + g_idata[i1+blockDim.x] + g_idata[i2+blockDim.x] + g_idata[i3+blockDim.x];
		gtdata[idx+blockDim.x] = table[accu];
		accu = g_idata[i0+2*blockDim.x] + g_idata[i1+2*blockDim.x] + g_idata[i2+2*blockDim.x] + g_idata[i3+2*blockDim.x];
		gtdata[idx+2*blockDim.x] = table[accu];
		accu = g_idata[i0+3*blockDim.x] + g_idata[i1+3*blockDim.x] + g_idata[i2+3*blockDim.x] + g_idata[i3+3*blockDim.x];
		gtdata[idx+3*blockDim.x] = table[accu];
		accu = g_idata[i0+4*blockDim.x] + g_idata[i1+4*blockDim.x] + g_idata[i2+4*blockDim.x] + g_idata[i3+4*blockDim.x];
		gtdata[idx+4*blockDim.x] = table[accu];
		accu = g_idata[i0+5*blockDim.x] + g_idata[i1+5*blockDim.x] + g_idata[i2+5*blockDim.x] + g_idata[i3+5*blockDim.x];
		gtdata[idx+5*blockDim.x] = table[accu];
		accu = g_idata[i0+6*blockDim.x] + g_idata[i1+6*blockDim.x] + g_idata[i2+6*blockDim.x] + g_idata[i3+6*blockDim.x];
		gtdata[idx+6*blockDim.x] = table[accu];
		accu = g_idata[i0+7*blockDim.x] + g_idata[i1+7*blockDim.x] + g_idata[i2+7*blockDim.x] + g_idata[i3+7*blockDim.x];
		gtdata[idx+7*blockDim.x] = table[accu];

		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// binary entropy, wsize = 5
void binogini5_2(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int accu;
	int tmp, tmp1, tmp2, tmp3, tmp4;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*5] * N;
		tmp1 = col_id[i*5+1] * N;
		tmp2 = col_id[i*5+2] * N;
		tmp3 = col_id[i*5+3] * N;
		tmp4 = col_id[i*5+4] * N;
		for (int j = 0; j < n; j++) {
			accu = data[tmp+j] + data[tmp1+j] + data[tmp2+j] + data[tmp3+j] + data[tmp4+j]; // column major
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini5_2(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 5 * blockIdx.y;
		unsigned int accu;
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		accu = g_idata[i0] + g_idata[i1] + g_idata[i2] + g_idata[i3] + g_idata[i4];
		gtdata[idx] = table[accu];
		accu = g_idata[i0+blockDim.x] + g_idata[i1+blockDim.x] + g_idata[i2+blockDim.x] + g_idata[i3+blockDim.x] + g_idata[i4+blockDim.x];
		gtdata[idx+blockDim.x] = table[accu];
		accu = g_idata[i0+2*blockDim.x] + g_idata[i1+2*blockDim.x] + g_idata[i2+2*blockDim.x] + g_idata[i3+2*blockDim.x] + g_idata[i4+2*blockDim.x];
		gtdata[idx+2*blockDim.x] = table[accu];
		accu = g_idata[i0+3*blockDim.x] + g_idata[i1+3*blockDim.x] + g_idata[i2+3*blockDim.x] + g_idata[i3+3*blockDim.x] + g_idata[i4+3*blockDim.x];
		gtdata[idx+3*blockDim.x] = table[accu];
		accu = g_idata[i0+4*blockDim.x] + g_idata[i1+4*blockDim.x] + g_idata[i2+4*blockDim.x] + g_idata[i3+4*blockDim.x] + g_idata[i4+4*blockDim.x];
		gtdata[idx+4*blockDim.x] = table[accu];
		accu = g_idata[i0+5*blockDim.x] + g_idata[i1+5*blockDim.x] + g_idata[i2+5*blockDim.x] + g_idata[i3+5*blockDim.x] + g_idata[i4+5*blockDim.x];
		gtdata[idx+5*blockDim.x] = table[accu];
		accu = g_idata[i0+6*blockDim.x] + g_idata[i1+6*blockDim.x] + g_idata[i2+6*blockDim.x] + g_idata[i3+6*blockDim.x] + g_idata[i4+6*blockDim.x];
		gtdata[idx+6*blockDim.x] = table[accu];
		accu = g_idata[i0+7*blockDim.x] + g_idata[i1+7*blockDim.x] + g_idata[i2+7*blockDim.x] + g_idata[i3+7*blockDim.x] + g_idata[i4+7*blockDim.x];
		gtdata[idx+7*blockDim.x] = table[accu];

		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

// binary entropy, wsize = 9
void binogini9_2(double* ent, int* data, int* col_id, int batch_len, int n, int N, double* table)
{
	int accu;
	int tmp, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id[i*9] * N;
		tmp1 = col_id[i*9+1] * N;
		tmp2 = col_id[i*9+2] * N;
		tmp3 = col_id[i*9+3] * N;
		tmp4 = col_id[i*9+4] * N;
		tmp5 = col_id[i*9+5] * N;
		tmp6 = col_id[i*9+6] * N;
		tmp7 = col_id[i*9+7] * N;
		tmp8 = col_id[i*9+8] * N;
		for (int j = 0; j < n; j++) {
			accu = data[tmp+j] + data[tmp1+j] + data[tmp2+j] + data[tmp3+j] + data[tmp4+j]\
					+ data[tmp5+j] + data[tmp6+j] + data[tmp7+j] + data[tmp8+j]; // column major
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini9_2(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y*N; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = 9 * blockIdx.y;
		unsigned int accu;
		unsigned int i0 = g_col_id[tmp]*N + idx;
		unsigned int i1 = g_col_id[tmp+1]*N + idx;
		unsigned int i2 = g_col_id[tmp+2]*N + idx;
		unsigned int i3 = g_col_id[tmp+3]*N + idx;
		unsigned int i4 = g_col_id[tmp+4]*N + idx;
		unsigned int i5 = g_col_id[tmp+5]*N + idx;
		unsigned int i6 = g_col_id[tmp+6]*N + idx;
		unsigned int i7 = g_col_id[tmp+7]*N + idx;
		unsigned int i8 = g_col_id[tmp+8]*N + idx;
		accu = g_idata[i0] + g_idata[i1] + g_idata[i2] + g_idata[i3] + g_idata[i4] + g_idata[i5] + g_idata[i6] + g_idata[i7] + g_idata[i8];
		gtdata[idx] = table[accu];
		accu = g_idata[i0+blockDim.x] + g_idata[i1+blockDim.x] + g_idata[i2+blockDim.x] + g_idata[i3+blockDim.x] + g_idata[i4+blockDim.x]\
				+ g_idata[i5+blockDim.x] + g_idata[i6+blockDim.x] + g_idata[i7+blockDim.x] + g_idata[i8+blockDim.x];
		gtdata[idx+blockDim.x] = table[accu];
		accu = g_idata[i0+2*blockDim.x] + g_idata[i1+2*blockDim.x] + g_idata[i2+2*blockDim.x] + g_idata[i3+2*blockDim.x] + g_idata[i4+2*blockDim.x]\
				+ g_idata[i5+2*blockDim.x] + g_idata[i6+2*blockDim.x] + g_idata[i7+2*blockDim.x] + g_idata[i8+2*blockDim.x];
		gtdata[idx+2*blockDim.x] = table[accu];
		accu = g_idata[i0+3*blockDim.x] + g_idata[i1+3*blockDim.x] + g_idata[i2+3*blockDim.x] + g_idata[i3+3*blockDim.x] + g_idata[i4+3*blockDim.x]\
				+ g_idata[i5+3*blockDim.x] + g_idata[i6+3*blockDim.x] + g_idata[i7+3*blockDim.x] + g_idata[i8+3*blockDim.x];
		gtdata[idx+3*blockDim.x] = table[accu];
		accu = g_idata[i0+4*blockDim.x] + g_idata[i1+4*blockDim.x] + g_idata[i2+4*blockDim.x] + g_idata[i3+4*blockDim.x] + g_idata[i4+4*blockDim.x]\
				+ g_idata[i5+4*blockDim.x] + g_idata[i6+4*blockDim.x] + g_idata[i7+4*blockDim.x] + g_idata[i8+4*blockDim.x];
		gtdata[idx+4*blockDim.x] = table[accu];
		accu = g_idata[i0+5*blockDim.x] + g_idata[i1+5*blockDim.x] + g_idata[i2+5*blockDim.x] + g_idata[i3+5*blockDim.x] + g_idata[i4+5*blockDim.x]\
				+ g_idata[i5+5*blockDim.x] + g_idata[i6+5*blockDim.x] + g_idata[i7+5*blockDim.x] + g_idata[i8+5*blockDim.x];
		gtdata[idx+5*blockDim.x] = table[accu];
		accu = g_idata[i0+6*blockDim.x] + g_idata[i1+6*blockDim.x] + g_idata[i2+6*blockDim.x] + g_idata[i3+6*blockDim.x] + g_idata[i4+6*blockDim.x]\
				+ g_idata[i5+6*blockDim.x] + g_idata[i6+6*blockDim.x] + g_idata[i7+6*blockDim.x] + g_idata[i8+6*blockDim.x];
		gtdata[idx+6*blockDim.x] = table[accu];
		accu = g_idata[i0+7*blockDim.x] + g_idata[i1+7*blockDim.x] + g_idata[i2+7*blockDim.x] + g_idata[i3+7*blockDim.x] + g_idata[i4+7*blockDim.x]\
				+ g_idata[i5+7*blockDim.x] + g_idata[i6+7*blockDim.x] + g_idata[i7+7*blockDim.x] + g_idata[i8+7*blockDim.x];
		gtdata[idx+7*blockDim.x] = table[accu];

		// unrolling 8
		double a1 = gtdata[idx];
		double a2 = gtdata[idx+ blockDim.x];
		double a3 = gtdata[idx+ 2*blockDim.x];
		double a4 = gtdata[idx+ 3*blockDim.x];
		double b1 = gtdata[idx+ 4*blockDim.x];
		double b2 = gtdata[idx+ 5*blockDim.x];
		double b3 = gtdata[idx+ 6*blockDim.x];
		double b4 = gtdata[idx+ 7*blockDim.x];
		gtdata[idx] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x*UNROLL;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

double mean(double* x, int d) 
{
	double accu = 0.0;
	for (int i = 0; i < d; i++) {
		accu += x[i];
	}
	return accu/((double) d);
}

// mean of a[b]
double mean_ab(double* a, int* b, int len)
{
	double accu = 0.0;
	for (int i = 0; i < len; i++) {
		accu += a[b[i]];
	}
	return accu/((double) len);
}

// mean of a[b[c]]
double mean_abc(double* a, int* b, int* c, int len)
{
	double accu = 0.0;
	for (int i = 0; i < len; i++) {
		accu += a[b[c[i]]];
	}
	return accu/((double) len);
}

// sum of a[b]
double sum_ab(double* a, int* b, int len)
{
	double accu = 0.0;
	for (int i = 0; i < len; i++) {
		accu += a[b[i]];
	}
	return accu;
}

// sum of a[b[c]]
double sum_abc(double* a, int* b, int* c, int len)
{
	double accu = 0.0;
	for (int i = 0; i < len; i++) {
		accu += a[b[c[i]]];
	}
	return accu;
}

bool and_bool(bool* a, int* idx, int len)
{
	for (int i = 0; i < len; i++) {
		if (!a[idx[i]]) {
			return false;
		}
	}
	return true;
}

bool or_bool(bool* a, int* idx, int len)
{
	for (int i = 0; i < len; i++) {
		if (a[idx[i]]) {
			return true;
		}
	}
	return false;
}

// uniform random number between 0 and N inclusive
int randomU(int N) 
{
	int r =  rand() & 255;
	r = r<<8 | (rand() & 255);
	r = r<<8 | (rand() & 255);
	r = r<<7 | (rand() & 127);
	return r % (N+1);
}

// uniform random number between 0 and 1
double randu()
{
	uint64_t r = rand() & 255;
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	r = r << 8 | (rand() & 255);
	return ((double) r)/((double) ULLINT_MAX);
}

// random permutation of idx of size len, Durstenfeld's algorithm
void randperm(int* idx, int len) 
{
	int j;
	int junk;
	for (int i = len - 1; i > 0; i--) {
		j = randomU(i);
		junk = idx[i];
		idx[i] = idx[j];
		idx[j] = junk;
	}
}

// random permutation of idx and data of size len, Durstenfeld's algorithm
void randperm_ab(int* idx, int64_t* data, int len) 
{
	int j;
	int junk;
	int64_t djunk;
	
	for (int i = len - 1; i > 0; i--) {
		j = randomU(i);
		junk = idx[i];
		idx[i] = idx[j];
		idx[j] = junk;
		djunk = data[i];
		data[i] = data[j];
		data[j] = djunk;
	}
}

// find maximum element in data
int maxint(int* data, int len)
{
	int vmax = data[0];
	for(int i = 1; i < len; i++) {
		vmax = vmax < data[i] ? data[i] : vmax;
	}
	return vmax;
}

// find maximum of a(b)
int maxint_ab(int64_t* a, int* b, int len)
{
	int64_t vmax = a[b[0]];
	for (int i = 0; i < len; i++) {
		vmax = vmax < a[b[i]] ? a[b[i]] : vmax;
	}
	return vmax;
}

// find minimum element in data
int minint(int* data, int len)
{
	int vmin = data[0];
	for(int i = 0; i < len; i++) {
		vmin = vmin > data[i] ? data[i] : vmin;
	}
	return vmin;
}
