#ifndef _TOPOLOGY_H
#define _TOPOLOGY_H

void findwindow1d(int*, int, int, int);

void findwindow(int*, int, int, int, int);

int mod(int, int);

int findaffected1d(int*, int, int, int, int);

int findaffected(int*, int, int, int, int, int);

int findaffectedblock(int*, int, int, int, int, int, int);

void blockshuffle(int*, int, int, int*, int);

void blockswap(int*, int*, int, int, int, int);

void multinomialent(double*, int*, int*, int*, int, int, int, int, int, double*);

__global__ void mulnom(double*, int*, double*, int*, int, int, int, double*);

void multinomialent3_3(double*, int*, int*, int, int, int, double*);

__global__ void mulnom3_3(double*, int*, double*, int*, int, double*);

void multinomialent4_3(double*, int*, int*, int, int, int, double*);

__global__ void mulnom4_3(double*, int*, double*, int*, int, double*);

void multinomialent5_3(double*, int*, int*, int, int, int, double*);

__global__ void mulnom5_3(double*, int*, double*, int*, int, double*);

void multinomialent3_4(double*, int*, int*, int, int, int, double*);

__global__ void mulnom3_4(double*, int*, double*, int*, int, double*);

void multinomialent4_4(double*, int*, int*, int, int, int, double*);

__global__ void mulnom4_4(double*, int*, double*, int*, int, double*);

void multinomialent5_4(double*, int*, int*, int, int, int, double*);

__global__ void mulnom5_4(double*, int*, double*, int*, int, double*);

void multinomialent3_5(double*, int*, int*, int, int, int, double*);

__global__ void mulnom3_5(double*, int*, double*, int*, int, double*);

void multinomialent4_5(double*, int*, int*, int, int, int, double*);

__global__ void mulnom4_5(double*, int*, double*, int*, int, double*);

__global__ void mulnom5_5(double*, int*, double*, int*, int, double*);

__global__ void mulnom3_6(double*, int*, double*, int*, int, double*);

__global__ void mulnom4_6(double*, int*, double*, int*, int, double*);

__global__ void mulnom5_6(double*, int*, double*, int*, int, double*);

__global__ void mulnom3_7(double*, int*, double*, int*, int, double*);

__global__ void mulnom4_7(double*, int*, double*, int*, int, double*);

__global__ void mulnom5_7(double*, int*, double*, int*, int, double*);

void binogini(double*, int*, int*, int, int, int, int, double*);

__global__ void bingini(double*, int*, double*, int*, int, int, double*);

void binogini3_2(double*, int*, int*, int, int, int, double*);

__global__ void bingini3_2(double*, int*, double*, int*, int, double*);

void binogini4_2(double*, int*, int*, int, int, int, double*);

__global__ void bingini4_2(double*, int*, double*, int*, int, double*);

void binogini5_2(double*, int*, int*, int, int, int, double*);

__global__ void bingini5_2(double*, int*, double*, int*, int, double*);

void binogini9_2(double*, int*, int*, int, int, int, double*);

__global__ void bingini9_2(double*, int*, double*, int*, int, double*);

double mean(double*, int);

double mean_ab(double*, int*, int);

double mean_abc(double*, int*, int*, int);

double sum_ab(double*, int*, int);

double sum_abc(double*, int*, int*, int);

bool and_bool(bool*, int*, int);

bool or_bool(bool*, int*, int);

int randomU(int);

double randu();

void randperm(int*, int);

void randperm_ab(int*, int64_t*, int len);

int maxint(int*, int);

int maxint_ab(int64_t*, int*, int);

int minint(int*, int);

#endif