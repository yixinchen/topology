#!/bin/bash
module load cuda10.1/toolkit
echo "nvcc -x cu -O2 -Xptxas -O3 2-Opt-clique.cu -o 2OPTG"
nvcc -x cu -O2 -Xptxas -O3 2-Opt-clique.cu -o 2OPTG
echo "mv 2OPTG ../bin/"
mv 2OPTG ../bin/
