#!/bin/bash
module load cuda10.1/toolkit
echo "nvcc -O1 -c topDMGPU.cu -o topDMGPU.obj"
nvcc -O1 -c topDMGPU.cu -o topDMGPU.obj
echo "nvcc -o topDMGPU topDMGPU.obj"
nvcc -o topDMGPU topDMGPU.obj
echo "rm topDMGPU.obj"
echo "mv topDMGPU ../bin/"
rm topDMGPU.obj
mv topDMGPU ../bin/
