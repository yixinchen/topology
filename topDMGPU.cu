#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>

void multinomialent(double*, int*, int*, int, int, int, int, double*);

__global__ void mulnom(double*, int*, double*, int*, int, int, double*);

void binogini(double*, int*, int*, int, int, int, int, double*);

__global__ void bingini(double*, int*, double*, int*, int, int, double*);

void binogini_s(double*, int*, int*, int, int, int, int, double*);

void multinomialent_s(double*, int*, int*, int, int, int, int, double*);

__global__ void bingini_s(double*, int*, int*, double*, int*, int*, int, int, double*);

__global__ void mulnom_s(double*, int*, int*, double*, int*, int*, int, int, double*);

int maxint(int*, int);

int minint(int*, int);

const int UNROLL = 8; // if changed to a different value, kernel code needs to be changed accordingly
const int CPU_NONBINARY_OTHER = 100;	// use CPU binogini
const int CPU_BINARY_OTHER = -1;		// use CPU multinomialent
const int GPU_NONBINARY_OTHER = 1100;	// use GPU bingini
const int GPU_BINARY_OTHER = -11;		// use GPU mulnom
const int MAX_DIM = 65535;				// maximum input dimension
const int MAX_BASE = 65535;				// maximum number of discrete feature values

int main(int argc, char **argv)
{
	int err = 0;
	char* ifname;
	char* ofname;
	int dimx = 512; 					// note that number of threads per block dimx * dimy <= 1024
	int wsize = 2;						// window size
	int dim = 1;						// dimension of topology
	int sparse = 0;						// sparse features, 0 not sparse
	bool iflag = false;
	bool oflag = false;
	bool bflag = true;
	bool dflag = true;
	bool gflag = true;
	bool sflag = true;
	int cpu = 0;
	static char usage[] = "usage: %s [-bdgh] -i ifname -o ofname\n";
	int ac = 1;	// argument count

	while (ac < argc) {
		if (!strcmp(argv[ac], "-h")) {
			printf(" \n");
			printf(usage, argv[0]);
			printf(" \n");
			printf("-i		input file name\n");
			printf("-o		output file name\n");
			printf("-b		x dimension of a cuda block, needs to be a power of 2 no greater than 1024, default value is 512\n");
			printf("-d		dimension of topology, default value is 1\n");
			printf("-g		gpu or cpu: 0 (default, cpu+gpu when available), 1 (cpu)\n");
			printf("-s		sparse features: 0 (default not sparse), 1 (sparse)\n");
			printf("-h		help\n");
			return 1;
		} else if (!strcmp(argv[ac], "-i")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ifname = argv[ac];
				iflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-o")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ofname = argv[ac];
				oflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-b")) {
			ac++;
			bflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				dimx = (int) pow(2.0, log( (double) (atoi(argv[ac]) % 1025)) / log(2.0));
				bflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-d")) {
			ac++;
			dflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				dim = atoi(argv[ac]);
				dflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-g")) {
			ac++;
			gflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				cpu = atoi(argv[ac]) != 0 ? 1 : 0;
				gflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-s")) {
			ac++;
			sflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				sparse = atoi(argv[ac]);
				sflag = true;
			}
			else {
				break;
			}
		} else {
			err = 1;
			break;
		}
		ac++;
	}
	
	if (!iflag) {
		fprintf(stderr, "%s: missing '-i ifname' option\n", argv[0]);
		return 0;
	} else if (!oflag) {
		fprintf(stderr, "%s: missing '-o ofname' option\n", argv[0]);
		return 0;
	} else if (!bflag) {
		fprintf(stderr, "%s: missing argument for '-b'\n", argv[0]);
		return 0;
	} else if (!dflag) {
		fprintf(stderr, "%s: missing argument for '-d'\n", argv[0]);
		return 0;
	} else if (!gflag) {
		fprintf(stderr, "%s: missing argument for '-g'\n", argv[0]);
		return 0;
	} else if (!sflag) {
		fprintf(stderr, "%s: missing argument for '-s'\n", argv[0]);
		return 0;
	} else if (err == 1) {
		fprintf(stderr, usage, argv[0]);
		return 0;
	}
	
	printf("%s Starting...\n", argv[0]);
	printf("Input file:                          %s\n",ifname);
	printf("Output file:                         %s\n",ofname);
	printf("x dimension of each cuda block:      %d\n", dimx);
	printf("Topology dimension:                  %d\n", dim);
	printf("Sparse features:                     %d\n", sparse);
	
	FILE *ifile_ptr;
	ifile_ptr = fopen(ifname,"rb");
	if (ifile_ptr == NULL) {
		printf("Could not open input file %s.\n", ifname);
		return -1;
	}
	FILE *ofile_ptr;
	ofile_ptr = fopen(ofname,"wb");
	if (ofile_ptr == NULL) {
		printf("Could not open input file %s.\n", ofname);
		return -1;
	}

	int n, d, base;	// n sample size, d features, base number of values of each features
	int w, h;		// w width, h height, for 2d only
	if (dim == 1) {	// 1D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&base, sizeof(int), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}
	else {			// 2D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&w, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&h, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&base, sizeof(int), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}

	printf("Number of samples is                 %d\n", n);
	printf("Number of features is                %d\n", d);
	if (dim == 2) {	// 2D topology
		printf("Width of image is                    %d\n", w);
		printf("Height of image is                   %d\n", h);
	}
	printf("Number of discrete values is         %d\n", base);

	if (base > MAX_BASE) {
		printf("Number of discrete values in input file should not exceed %d.\n", MAX_BASE);
		return -1;
	}
	if (d > MAX_DIM) {
		printf("Feature dimension can not exceed %d.\n", MAX_DIM);
		return -1;
	}
	if ((dim < 1) || (dim > 2)) {
		printf("Topology dimension must be 1 or 2.\n");
		return -1;
	}
	if (dimx == 0) {
		printf("x dimension of each block must be a power of 2, larger than 1 and smaller than 1025.\n");
		return -1;
	}
	if ((dim == 2) && (d != w*h)) {
		printf("dimension does not match width*height.\n");
		return -1;
	}

	int devices = 0; 
	bool gpu = false;
	cudaError_t c_err = cudaGetDeviceCount(&devices); 
	
	// find GPU.
	if (cpu == 0) {
		if (devices > 0 && c_err == cudaSuccess) {
			gpu = true;
		}
		else {
			gpu = false;
			devices = 0;
		}
	} else {
		devices = 0;
	}
	printf("Running on                           %s, devices = %d \n", gpu ? "GPU + CPU" : "CPU", devices);
	
	int option = 0;
	if (devices == 0) {
		switch(base) {
		case 2 :
			option = CPU_BINARY_OTHER;
			break;
		default :
			option = CPU_NONBINARY_OTHER;
		}
	}
	else {
		switch(base) {
		case 2 :
			option = GPU_BINARY_OTHER;
			break;
		default :
			option = GPU_NONBINARY_OTHER;
		}
	}

	int* h_idx;		// features indices before swaps
	h_idx = (int *)malloc(d*sizeof(int));
	if (h_idx == NULL) {
		printf("Memory allocation for h_idx failed\n");
		return -1;
	}
	if (fread(h_idx, sizeof(int), d, ifile_ptr) != d) {
		printf("An error occurred reading feature indices.\n");
		return -1;
	}

	int* h_data;	// host data pointer
	int dimy = 1;
	dim3 block (dimx, dimy);
	int N = (n + dimx * UNROLL - 1) / (dimx * UNROLL) * dimx * UNROLL; // size of each sample after zero padding
	dim3 grid ((N+block.x-1)/block.x, 1);
	dim3 truegrid(grid.x/UNROLL,1);			// taking into account unrolling by 8
	int64_t NXY = N*d; // number of elements after zero padding
	int64_t nBytes = NXY * sizeof(int);
	int64_t NBytes = NXY * sizeof(double);
	printf("Needs %lld Bytes for %lld elements. The closest multiple of (dimx * UNROLL) upperbound on the number of samples is %d\n", nBytes, NXY, N);
	// allocate host memory
	h_data = (int *)calloc(NXY, sizeof(int)); // must initialize to 0
	if (h_data == NULL) {
		printf("Memory allocation for h_data failed\n");
		return -1;
	}
	
	// column major
	for (int i = 0; i < d; i++) {
		if (fread(&h_data[i*N], sizeof(int), n, ifile_ptr) != n) {
			printf("Error reading data at i=%d.\n",i);
			return -1;
		}
	}
	fclose(ifile_ptr);
	if( (maxint(h_data, NXY) >= base) || (minint(h_data, NXY) < 0)) {
		printf("Input file discrete values should stay within 0 and %d, inclusive.\n", base-1);
		return -1;
	}

	// allocate device memory
	int* d_idata = NULL;
	double* d_tdata = NULL; // temporay storage for window ents
	int* bg_cnt = NULL;
	double* d_odata = NULL;	// partial sum of window ents in a batch, column major
	int* c_odata = NULL;
	int* d_idx_tmp = NULL; // idx_tmp on device
	double* h_odata = NULL;
	int* hc_odata = NULL;
	if (gpu) {
		if (cudaMalloc((void **) &d_idata, nBytes) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		if (cudaMalloc((void **) &d_tdata, NBytes / UNROLL) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		if (cudaMalloc((void **) &bg_cnt, nBytes / UNROLL) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		if (cudaMalloc((void **) &d_odata, truegrid.x * d * sizeof(double)) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		if (cudaMalloc((void **) &c_odata, truegrid.x * d * sizeof(int)) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		if (cudaMalloc((void **) &d_idx_tmp, d * wsize * sizeof(int)) != cudaSuccess) {
			printf("GPU memoory allocation failed\n");
			return -1;
		}
		// allocate host memory for partial sum of window ents in a batch
		if (cudaMallocHost((void**)&h_odata, truegrid.x * d * sizeof(double)) != cudaSuccess) {	// pinned memory for d_odata
			printf("Pinned memoory allocation failed\n");
			return -1;
		}
		if (cudaMallocHost((void**)&hc_odata, truegrid.x * d * sizeof(int)) != cudaSuccess) { // pinned memory for c_odata
			printf("Pinned memoory allocation failed\n");
			return -1;
		}
		// send data to GPU
		cudaMemcpy(d_idata, h_data, nBytes, cudaMemcpyHostToDevice);
	}

	double* ent_ori;								// entropy of original patches
	int64_t num_neighbors = d*(d-1)/2;					// number of neighbors for wsize 2
	ent_ori = (double *)calloc(num_neighbors, sizeof(double));	// initialize to 0
	if (ent_ori == NULL) {
		printf("Memory allocation for ent_ori failed\n");
		return -1;
	}

	int* col_id;								// grid ids in each batch, each affected window contains wsize grids
	col_id = (int *)calloc(d * wsize, sizeof(int));		// initialize to 0
	double* ent_table;		// table to save numbers for entropy calculation
	ent_table = (double *)calloc((wsize+1), sizeof(double)); // initialize table
	for (int i = 1; i < wsize; i++) {
		ent_table[i] = -((double) i)/((double) wsize) * log((double)(i)/((double) wsize)) / log((double) base);
	}
	double* berent_table;		// table to save numbers for Bernoulli entropy calculation
	berent_table = (double *)calloc((wsize+1), sizeof(double));	// initialize table
	for (int i = 1; i < wsize; i++) {
		//berent_table[i] = ((double) (i * (wsize - i)))/((double) (wsize * wsize)); // Gini impurity
		double tmp = ((double) i)/((double) wsize);
		berent_table[i] = -(tmp * log(tmp) + (1.0-tmp) * log(1.0-tmp) )/ log(2.0);
	}
	
	int* idx_tmp;
	double* d_ent_table = NULL;
	double* d_berent_table = NULL;
	if (gpu) {
		cudaMallocHost((void**)&idx_tmp, d * wsize * sizeof(int)); // pinned memory for d_idx_tmp
		// allocate ent table on device
		cudaMalloc((void **) &d_ent_table, (wsize+1) * sizeof(double));
		cudaMemcpy(d_ent_table, ent_table, (wsize+1) * sizeof(double), cudaMemcpyHostToDevice);
		cudaMalloc((void **) &d_berent_table, (wsize+1) * sizeof(double));
		cudaMemcpy(d_berent_table, berent_table, (wsize+1) * sizeof(double), cudaMemcpyHostToDevice);
	} else {
		idx_tmp = (int *)malloc(d * wsize * sizeof(int));
	}

	// Start measuring time
	clock_t begin = clock();
	if (sparse == 0) {
		switch(option) {
		case CPU_BINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					col_id[wsize * (j - i - 1)] = i;
					col_id[wsize * (j - i - 1) + 1] = j;
				}
				binogini(ent_ori + i * d - i * (i + 1)/2, h_data, col_id, d-1-i, n, N, wsize, berent_table); // column major
				printf(".");
			}
			break;
		case GPU_BINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					idx_tmp[wsize * (j - i - 1)] = i;
					idx_tmp[wsize * (j - i - 1) + 1] = j;
				}
				cudaMemcpy(d_idx_tmp, idx_tmp, wsize * (d - 1 - i) * sizeof(int), cudaMemcpyHostToDevice);
				truegrid.y = d - 1 - i;
				bingini <<<truegrid,block>>> (d_odata, d_idata, d_tdata, d_idx_tmp, N, wsize, d_berent_table);
				cudaDeviceSynchronize();
				cudaMemcpy(h_odata, d_odata, truegrid.y * truegrid.x * sizeof(double), cudaMemcpyDeviceToHost);
				for (int j = 0; j < d-1-i; j++) {
					ent_ori[i*d-i*(i+1)/2+j] = 0;
					for (int k = 0; k < truegrid.x; k++) {
						ent_ori[i*d-i*(i+1)/2+j] += h_odata[j*truegrid.x + k];
					}
				}
				printf("*");
			}
			break;
		case CPU_NONBINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					col_id[wsize * (j - i - 1)] = i;
					col_id[wsize * (j - i - 1) + 1] = j;
				}
				multinomialent(ent_ori + i * d - i * (i + 1)/2, h_data, col_id, d-1-i, n, N, base, ent_table); // column major
				printf(".");
			}
			break;
		case GPU_NONBINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					idx_tmp[wsize * (j - i - 1)] = i;
					idx_tmp[wsize * (j - i - 1) + 1] = j;
				}
				cudaMemcpy(d_idx_tmp, idx_tmp, wsize * (d - 1 - i) * sizeof(int), cudaMemcpyHostToDevice);
				truegrid.y = d - 1 - i;
				mulnom <<<truegrid,block>>> (d_odata, d_idata, d_tdata, d_idx_tmp, N, base, d_ent_table);
				cudaDeviceSynchronize();
				cudaMemcpy(h_odata, d_odata, truegrid.y * truegrid.x * sizeof(double), cudaMemcpyDeviceToHost);
				for (int j = 0; j < d-1-i; j++) {
					ent_ori[i*d-i*(i+1)/2+j] = 0;
					for (int k = 0; k < truegrid.x; k++) {
						ent_ori[i*d-i*(i+1)/2+j] += h_odata[j*truegrid.x + k];
					}
				}
				printf(".");
			}
		}
		for (int i = 0; i < num_neighbors; i++) {
			ent_ori[i] = ent_ori[i]/((double) n);
		}
	}
	else {
		switch(option) {
		case CPU_BINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					col_id[wsize * (j - i - 1)] = i;
					col_id[wsize * (j - i - 1) + 1] = j;
				}
				binogini_s(ent_ori + i * d - i * (i + 1)/2, h_data, col_id, d-1-i, n, N, wsize, berent_table); // column major
				printf(".");
			}
			break;
		case GPU_BINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					idx_tmp[wsize * (j - i - 1)] = i;
					idx_tmp[wsize * (j - i - 1) + 1] = j;
				}
				cudaMemcpy(d_idx_tmp, idx_tmp, wsize * (d - 1 - i) * sizeof(int), cudaMemcpyHostToDevice);
				truegrid.y = d - 1 - i;
				bingini_s <<<truegrid,block>>> (d_odata, c_odata, d_idata, d_tdata, d_idx_tmp, bg_cnt, N, wsize, d_berent_table);
				cudaDeviceSynchronize();
				cudaMemcpy(h_odata, d_odata, truegrid.y * truegrid.x * sizeof(double), cudaMemcpyDeviceToHost);
				cudaMemcpy(hc_odata, c_odata, truegrid.y * truegrid.x * sizeof(int), cudaMemcpyDeviceToHost);
				for (int j = 0; j < d-1-i; j++) {
					ent_ori[i*d-i*(i+1)/2+j] = 0;
					int tmp = 0;
					for (int k = 0; k < truegrid.x; k++) {
						ent_ori[i*d-i*(i+1)/2+j] += h_odata[j*truegrid.x + k];
						tmp += hc_odata[j*truegrid.x + k];
					}
					ent_ori[i*d-i*(i+1)/2+j] = (tmp == 0) ? ((double) 0): ent_ori[i*d-i*(i+1)/2+j] / tmp;
				}
				printf(".");
			}
			break;
		case CPU_NONBINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					col_id[wsize * (j - i - 1)] = i;
					col_id[wsize * (j - i - 1) + 1] = j;
				}
				multinomialent_s(ent_ori + i * d - i * (i + 1)/2, h_data, col_id, d-1-i, n, N, base, ent_table); // column major
				printf(".");
			}
			break;
		case GPU_NONBINARY_OTHER :
			for (int i = 0; i < d-1; i++) {
				for (int j = i+1; j < d; j++) {
					idx_tmp[wsize * (j - i - 1)] = i;
					idx_tmp[wsize * (j - i - 1) + 1] = j;
				}
				cudaMemcpy(d_idx_tmp, idx_tmp, wsize * (d - 1 - i) * sizeof(int), cudaMemcpyHostToDevice);
				truegrid.y = d - 1 - i;
				mulnom_s <<<truegrid,block>>> (d_odata, c_odata, d_idata, d_tdata, d_idx_tmp, bg_cnt, N, base, d_ent_table);
				cudaDeviceSynchronize();
				cudaMemcpy(h_odata, d_odata, truegrid.y * truegrid.x * sizeof(double), cudaMemcpyDeviceToHost);
				cudaMemcpy(hc_odata, c_odata, truegrid.y * truegrid.x * sizeof(int), cudaMemcpyDeviceToHost);
				for (int j = 0; j < d-1-i; j++) {
					ent_ori[i*d-i*(i+1)/2+j] = 0;
					int tmp = 0;
					for (int k = 0; k < truegrid.x; k++) {
						ent_ori[i*d-i*(i+1)/2+j] += h_odata[j*truegrid.x + k];
						tmp += hc_odata[j*truegrid.x + k];
					}
					ent_ori[i*d-i*(i+1)/2+j] = (tmp == 0) ? ((double) 0) : ent_ori[i*d-i*(i+1)/2+j] / tmp;
				}
				printf(".");
			}
		}
	}
	clock_t end = clock();
	double  elapsed = (double) (end - begin) * 1e3 / CLOCKS_PER_SEC; // in milliseconds
	printf("\nTime measured: %.0f milliseconds.\n", elapsed);
	
	double ENT_ori = 0;
	double ENT = 0;
	if (dim == 1) {
		int s,e;
		for (int i = 0; i < d-1; i++) {
			ENT_ori += ent_ori[i * d - i * (i + 1)/2 ];
			s = h_idx[i];
			e = h_idx[i+1];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
		}
		ENT_ori = (ENT_ori + ent_ori[d-2]) / ((double) d);
		s = h_idx[d-1];
		e = h_idx[0];
		if (s > e) {
			int tmp = s;
			s = e;
			e = tmp;
		}
		ENT = (ENT + ent_ori[s * d - (s * s + s)/2 + e - s - 1]) / ((double) d);
	}
	else {
		int row,col,s,e;
		for (int i = 0; i < d; i++) {
			row = i % h;	// find row index of i
			col = i / h;	// find col index of i
			s = i;
			e = ((col + 1) % w) * h + row;	// index of the horizontal neighbor of i
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT_ori += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = h_idx[i];
			e = h_idx[((col + 1) % w) * h + row];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = i;
			e = col * h + ((row + 1) % h);	// index of the vertical neighbor of i
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT_ori += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = h_idx[i];
			e = h_idx[col * h + ((row + 1) % h)];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
		}
		ENT_ori = ENT_ori / (2.0 * ((double) d));
		ENT = ENT / (2.0 * ((double) d));
	}
	printf("ENT_ori = %.14f, ENT = %.14f\n", ENT_ori, ENT);
	
	// save new topology
	fwrite(&n, sizeof(int), 1, ofile_ptr);
	fwrite(&d, sizeof(int), 1, ofile_ptr);
	if (dim == 2) {
		fwrite(&w, sizeof(int), 1, ofile_ptr);
		fwrite(&h, sizeof(int), 1, ofile_ptr);
	}
	fwrite(&num_neighbors, sizeof(int64_t), 1, ofile_ptr);
	fwrite(h_idx, sizeof(int), d, ofile_ptr);
	fwrite(ent_ori, sizeof(double), num_neighbors, ofile_ptr);
	fclose(ofile_ptr);

	free(h_idx);
	free(h_data);
	free(ent_ori);
	free(col_id);
	free(ent_table);
	free(berent_table);
	if (gpu) {
		cudaFree(d_idata);
		cudaFree(d_tdata);
		cudaFree(bg_cnt);
		cudaFree(d_odata);
		cudaFree(c_odata);
		cudaFree(d_idx_tmp);
		cudaFree(d_ent_table);
		cudaFree(d_berent_table);
		cudaFreeHost(idx_tmp);
		cudaFreeHost(h_odata);
		cudaFreeHost(hc_odata);
	} else {
		free(idx_tmp);
	}
	return 0;
}

// find maximum element in data
int maxint(int* data, int len)
{
	int vmax = data[0];
	for(int i = 1; i < len; i++) {
		vmax = vmax < data[i] ? data[i] : vmax;
	}
	return vmax;
}

// find minimum element in data
int minint(int* data, int len)
{
	int vmin = data[0];
	for(int i = 0; i < len; i++) {
		vmin = vmin > data[i] ? data[i] : vmin;
	}
	return vmin;
}

//void multinomialent(double* ent, int* data, int* col_id, int* hist, int batch_len, int n, int N, int base, double* table)
void multinomialent(double* ent, int* data, int* col_id, int batch_len, int n, int N, int base, double* table)
{
	int* tmpidx;
	double entval = 2 * table[1];
	
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmpidx = col_id + i * 2;
		for (int j = 0; j < n; j++) {
/*			for (int k = 0; k < base; k++) {
				hist[k] = 0;
			}
			for (int k = 0; k < wsize; k++) {
				hist[*(data + *(tmpidx +k)*N + j)] += 1; // column major
			}
			double tmp = table[hist[0]];
			for (int k = 1; k < base; k++) {
				tmp += table[hist[k]];
			}*/
			double tmp = (data[tmpidx[0]*N + j] == data[tmpidx[1]*N + j]) ? 0 : entval;
			ent[i] += tmp;
		}
	}
}

//__global__ void mulnom(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, int wsize, int base, double* g_table)
__global__ void mulnom(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, int base, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y * N / UNROLL; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;
	unsigned int idx0 = blockIdx.x * blockDim.x + tid;
	//int hist[MAX_BASE];
	double entval = 2.0 * g_table[1];
	
	if (idx + 7*blockDim.x < N) {
//		int tmp = wsize * blockIdx.y;
		int tmp = 2 * blockIdx.y;
		double a1 = (g_idata[g_col_id[tmp]*N + idx] == g_idata[g_col_id[tmp+1]*N + idx]) ? 0.0 : entval;
		double a2 = (g_idata[g_col_id[tmp]*N + idx + blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + blockDim.x]) ? 0.0 : entval;
		double a3 = (g_idata[g_col_id[tmp]*N + idx + 2*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 2*blockDim.x]) ? 0.0 : entval;
		double a4 = (g_idata[g_col_id[tmp]*N + idx + 3*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 3*blockDim.x]) ? 0.0 : entval;
		double b1 = (g_idata[g_col_id[tmp]*N + idx + 4*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 4*blockDim.x]) ? 0.0 : entval;
		double b2 = (g_idata[g_col_id[tmp]*N + idx + 5*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 5*blockDim.x]) ? 0.0 : entval;
		double b3 = (g_idata[g_col_id[tmp]*N + idx + 6*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 6*blockDim.x]) ? 0.0 : entval;
		double b4 = (g_idata[g_col_id[tmp]*N + idx + 7*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 7*blockDim.x]) ? 0.0 : entval;
/*		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx]] += 1;
		}
		double a1 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a1 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + blockDim.x]] += 1;
		}
		double a2 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a2 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x]] += 1;
		}
		double a3 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a3 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x]] += 1;
		}
		double a4 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a4 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x]] += 1;
		}
		double b1 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b1 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x]] += 1;
		}
		double b2 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b2 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x]] += 1;
		}
		double b3 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b3 += g_table[hist[i]];
		}
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x]] += 1;
		}
		double b4 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b4 += g_table[hist[i]];
		}*/
		// unrolling 8
		gtdata[idx0] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

void binogini(double* ent, int* data, int* col_id, int batch_len, int n, int N, int wsize, double* table)
{
	int accu;
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i * wsize;
		for (int j = 0; j < n; j++) {
			accu = 0;
			for (int k = 0; k < wsize; k++) {
				accu += data[*(tmp +k)*N + j];
			}
			ent[i] += table[accu];
		}
	}
}

__global__ void bingini(double* g_odata, int* g_idata, double* g_tdata, int* g_col_id, int N, int wsize, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y * N / UNROLL; // temporary ent results of one window
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;
	unsigned int idx0 = blockIdx.x * blockDim.x + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = wsize * blockIdx.y;
		unsigned int accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx];
		}
		double a1 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + blockDim.x];
		}
		double a2 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x];
		}
		double a3 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x];
		}
		double a4 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x];
		}
		double b1 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x];
		}
		double b2 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x];
		}
		double b3 = table[accu];
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x];
		}
		double b4 = table[accu];

		// unrolling 8
		gtdata[idx0] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
}

void binogini_s(double* ent, int* data, int* col_id, int batch_len, int n, int N, int wsize, double* table)
{
	int accu;
	int* tmp;
	int nonbackground;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i * wsize;
		nonbackground = 0;
		for (int j = 0; j < n; j++) {
			accu = 0;
			for (int k = 0; k < wsize; k++) {
				accu += data[*(tmp +k)*N + j];
			}
			if (accu > 0) { // ignore patches that are entirely background
				nonbackground++;
				ent[i] += table[accu];
			}
		}
		ent[i] = (nonbackground == 0) ? ((double) 0) : (ent[i] / ((double) nonbackground));
	}
}

//void multinomialent_s(double* ent, int* data, int* col_id, int* hist, int batch_len, int n, int N, int base, double* table)
void multinomialent_s(double* ent, int* data, int* col_id, int batch_len, int n, int N, int base, double* table)
{
	int* tmpidx;
	int nonbackground;
	double entval = 2 * table[1];

	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmpidx = col_id + i * 2;
		nonbackground = 0;
		for (int j = 0; j < n; j++) {
/*			for (int k = 0; k < base; k++) {
				hist[k] = 0;
			}
			for (int k = 0; k < wsize; k++) {
				hist[*(data + *(tmpidx +k)*N + j)] += 1; // column major
			}
			if (hist[0] != wsize) { // ignore patches that are entirely background
				double tmp = table[hist[0]];
				nonbackground++;
				for (int k = 1; k < base; k++) {
					tmp += table[hist[k]];
				}
				ent[i] += tmp;
			}*/
			if (data[tmpidx[0]*N + j] + data[tmpidx[1]*N + j] > 0) {
				nonbackground++;
				double tmp = (data[tmpidx[0]*N + j] == data[tmpidx[1]*N + j]) ? 0 : entval;
				ent[i] += tmp;
			}
		}
		ent[i] = (nonbackground == 0) ? ((double) 0) : (ent[i] / ((double) nonbackground));
	}
}

__global__ void bingini_s(double* g_odata, int* c_odata, int* g_idata, double* g_tdata, int* g_col_id, int* bg_cnt, int N, int wsize, double* table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y * N / UNROLL; // temporary ent results of one window
	int* bgcnt = bg_cnt  + blockIdx.y * N / UNROLL; // number of all 0's patches
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;
	unsigned int idx0 = blockIdx.x * blockDim.x + tid;

	if (idx + 7*blockDim.x < N) {
		int tmp = wsize * blockIdx.y;
		unsigned int accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx];
		}
		double a1 = table[accu];
		int c1 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + blockDim.x];
		}
		double a2 = table[accu];
		int c2 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x];
		}
		double a3 = table[accu];
		int c3 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x];
		}
		double a4 = table[accu];
		int c4 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x];
		}
		double b1 = table[accu];
		int d1 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x];
		}
		double b2 = table[accu];
		int d2 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x];
		}
		double b3 = table[accu];
		int d3 = (accu == 0) ? 0 : 1;
		accu = 0;
		for (int i = 0; i < wsize; i++) {
			accu += g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x];
		}
		double b4 = table[accu];
		int d4 = (accu == 0) ? 0 : 1;

		// unrolling 8
		gtdata[idx0] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
		bgcnt[idx0] = c1 + c2 + c3 + c4 + d1 + d2 + d3 + d4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x;
	int* cdata = bgcnt + blockIdx.x*blockDim.x;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
			cdata[tid] += cdata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}
	
	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		volatile int *vmem1 = cdata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
		vmem1[tid] += vmem1[tid + 32];
		vmem1[tid] += vmem1[tid + 16];
		vmem1[tid] += vmem1[tid + 8];
		vmem1[tid] += vmem1[tid + 4];
		vmem1[tid] += vmem1[tid + 2];
		vmem1[tid] += vmem1[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) {
		g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
		c_odata[gridDim.x * blockIdx.y + blockIdx.x] = cdata[0];
	}
}

__global__ void mulnom_s(double* g_odata, int* c_odata, int* g_idata, double* g_tdata, int* g_col_id, int* bg_cnt, int N, int base, double* g_table)
{
	unsigned int tid = threadIdx.x;
	double* gtdata = g_tdata + blockIdx.y * N / UNROLL; // temporary ent results of one window
	int* bgcnt = bg_cnt + blockIdx.y * N / UNROLL;
	unsigned int idx = blockIdx.x * blockDim.x * UNROLL + tid;
	unsigned int idx0 = blockIdx.x * blockDim.x + tid;
//	int hist[MAX_BASE];
	double entval = 2.0 * g_table[1];

	if (idx + 7*blockDim.x < N) {
//		int tmp = wsize * blockIdx.y;
		int tmp = 2 * blockIdx.y;
		double a1 = (g_idata[g_col_id[tmp]*N + idx] == g_idata[g_col_id[tmp+1]*N + idx]) ? 0.0 : entval;
		int c1 = (g_idata[g_col_id[tmp]*N + idx] + g_idata[g_col_id[tmp+1]*N + idx] == 0) ? 0 : 1;
		double a2 = (g_idata[g_col_id[tmp]*N + idx + blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + blockDim.x]) ? 0.0 : entval;
		int c2 = (g_idata[g_col_id[tmp]*N + idx + blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + blockDim.x] == 0) ? 0 : 1;
		double a3 = (g_idata[g_col_id[tmp]*N + idx + 2*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 2*blockDim.x]) ? 0.0 : entval;
		int c3 = (g_idata[g_col_id[tmp]*N + idx + 2*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 2*blockDim.x] == 0) ? 0 : 1;
		double a4 = (g_idata[g_col_id[tmp]*N + idx + 3*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 3*blockDim.x]) ? 0.0 : entval;
		int c4 = (g_idata[g_col_id[tmp]*N + idx + 3*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 3*blockDim.x] == 0) ? 0 : 1;
		double b1 = (g_idata[g_col_id[tmp]*N + idx + 4*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 4*blockDim.x]) ? 0.0 : entval;
		int d1 = (g_idata[g_col_id[tmp]*N + idx + 4*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 4*blockDim.x] == 0) ? 0 : 1;
		double b2 = (g_idata[g_col_id[tmp]*N + idx + 5*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 5*blockDim.x]) ? 0.0 : entval;
		int d2 = (g_idata[g_col_id[tmp]*N + idx + 5*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 5*blockDim.x] == 0) ? 0 : 1;
		double b3 = (g_idata[g_col_id[tmp]*N + idx + 6*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 6*blockDim.x]) ? 0.0 : entval;
		int d3 = (g_idata[g_col_id[tmp]*N + idx + 6*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 6*blockDim.x] == 0) ? 0 : 1;
		double b4 = (g_idata[g_col_id[tmp]*N + idx + 7*blockDim.x] == g_idata[g_col_id[tmp+1]*N + idx + 7*blockDim.x]) ? 0.0 : entval;
		int d4 = (g_idata[g_col_id[tmp]*N + idx + 7*blockDim.x] + g_idata[g_col_id[tmp+1]*N + idx + 7*blockDim.x] == 0) ? 0 : 1;
/*		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx]] += 1;
		}
		double a1 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a1 += g_table[hist[i]];
		}
		int c1 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + blockDim.x]] += 1;
		}
		double a2 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a2 += g_table[hist[i]];
		}
		int c2 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 2*blockDim.x]] += 1;
		}
		double a3 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a3 += g_table[hist[i]];
		}
		int c3  = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 3*blockDim.x]] += 1;
		}
		double a4 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			a4 += g_table[hist[i]];
		}
		int c4 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 4*blockDim.x]] += 1;
		}
		double b1 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b1 += g_table[hist[i]];
		}
		int d1 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 5*blockDim.x]] += 1;
		}
		double b2 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b2 += g_table[hist[i]];
		}
		int d2 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 6*blockDim.x]] += 1;
		}
		double b3 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b3 += g_table[hist[i]];
		}
		int d3 = (hist[0] == wsize) ? 0 : 1;
		for (int i = 0; i < base; i++) {
			hist[i] = 0;
		}
		for (int i = 0; i < wsize; i++) {
			hist[g_idata[g_col_id[tmp+i]*N + idx + 7*blockDim.x]] += 1;
		}
		double b4 = g_table[hist[0]];
		for (int i = 1; i < base; i++) {
			b4 += g_table[hist[i]];
		}
		int d4 = (hist[0] == wsize) ? 0 : 1;*/
		// unrolling 8
		gtdata[idx0] = a1 + a2 + a3 + a4 + b1 + b2 + b3 + b4;
		bgcnt[idx0] = c1 + c2 + c3 + c4 + d1 + d2 + d3 + d4;
	}
	__syncthreads();

	// convert global data pointer to the local pointer of this block
	double* idata = gtdata + blockIdx.x*blockDim.x;
	int* cdata = bgcnt + blockIdx.x*blockDim.x;
	
	// in-place reduction in global memory
	for (int stride = blockDim.x/2; stride > 32; stride >>=1) {
		if (tid < stride) {
			idata[tid] += idata[tid + stride];
			cdata[tid] += cdata[tid + stride];
		}
		// synchronize within threadblock
		__syncthreads();
	}

	// unrolling warp
	if (tid < 32) {
		volatile double *vmem = idata;
		volatile int *vmem1 = cdata;
		vmem[tid] += vmem[tid + 32];
		vmem[tid] += vmem[tid + 16];
		vmem[tid] += vmem[tid + 8];
		vmem[tid] += vmem[tid + 4];
		vmem[tid] += vmem[tid + 2];
		vmem[tid] += vmem[tid + 1];
		vmem1[tid] += vmem1[tid + 32];
		vmem1[tid] += vmem1[tid + 16];
		vmem1[tid] += vmem1[tid + 8];
		vmem1[tid] += vmem1[tid + 4];
		vmem1[tid] += vmem1[tid + 2];
		vmem1[tid] += vmem1[tid + 1];
	}
	
	// write result for this block to global mem
	if (tid == 0) {
		g_odata[gridDim.x * blockIdx.y + blockIdx.x] = idata[0];
		c_odata[gridDim.x * blockIdx.y + blockIdx.x] = cdata[0];
	}
}
