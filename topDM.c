// generate distance matrix
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define CPU_NONBINARY_OTHER 100			// use CPU binogini
#define CPU_BINARY_OTHER -1				// use CPU multinomialent
const int MAX_DIM = 65535;				// maximum input dimension
const int MAX_BASE = 65535;				// maximum number of discrete feature values

// find maximum element in data
int maxint(int* data, int len)
{
	int vmax = data[0];
	for(int i = 1; i < len; i++) {
		vmax = vmax < data[i] ? data[i] : vmax;
	}
	return vmax;
}

// find minimum element in data
int minint(int* data, int len)
{
	int vmin = data[0];
	for(int i = 0; i < len; i++) {
		vmin = vmin > data[i] ? data[i] : vmin;
	}
	return vmin;
}

// calculate distance matrix, 1-norm and 2-norm distances
void metricdistance(double* dist, int* data, int d, int n, int distancetype)
{
	double accu;
	int nonbackground;
	switch(distancetype) {
	case 1 :
		for (int i = 0; i < d-1; i++) {
			for (int j = i+1; j < d; j++) {
				nonbackground = 0;
				accu = 0;
				for (int k = 0; k < n; k++) {
					int a,b;
					a = data[i * n + k];
					b = data[j * n + k];
					if ((a != 0) || (b != 0)) {
						int delta = a - b;
						accu += (double) abs(delta);
						nonbackground++;
					}
				}
				dist[i * d - i * (i + 1)/2 + j - i - 1] = (nonbackground == 0) ? ((double) 0) : accu/((double) nonbackground);
			}
			printf(".");
		}
		break;
	case 2 :
		for (int i = 0; i < d-1; i++) {
			for (int j = i+1; j < d; j++) {
				nonbackground = 0;
				accu = 0;
				for (int k = 0; k < n; k++) {
					int a,b;
					a = data[i * n + k];
					b = data[j * n + k];
					if ((a != 0) || (b != 0)) {
						double delta = a - b;
						accu += delta * delta;
						nonbackground++;
					}
				}
				dist[i * d - i * (i + 1)/2 + j - i - 1] = (nonbackground == 0) ? ((double) 0) : sqrt(accu)/((double) nonbackground);
			}
			printf(".");
		}
	}
}

void binogini(double* ent, int* data, int* col_id, int batch_len, int n, int N, int wsize, double* table)
{
	int accu;
	int* tmp;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i * wsize;
		for (int j = 0; j < n; j++) {
			accu = 0;
			for (int k = 0; k < wsize; k++) {
				accu += data[*(tmp +k)*N + j];
			}
			ent[i] += table[accu];
		}
		ent[i] /= ((double) n);
	}
}

//void multinomialent(double* ent, int* data, int* col_id, int* hist, int batch_len, int n, int N, int wsize, int base, double* table)
void multinomialent(double* ent, int* data, int* col_id, int batch_len, int n, int N, int base, double* table)
{
	int* tmpidx;
	double entval = 2 * table[1];
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmpidx = col_id + i * 2;
		for (int j = 0; j < n; j++) {
			double tmp = (data[tmpidx[0]*N + j] == data[tmpidx[1]*N + j]) ? 0 : entval;
			ent[i] += tmp;
		}
		ent[i] /= ((double) n);
	}
}

void binogini_s(double* ent, int* data, int* col_id, int batch_len, int n, int N, int wsize, double* table)
{
	int accu;
	int* tmp;
	int nonbackground;
	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmp = col_id + i * wsize;
		nonbackground = 0;
		for (int j = 0; j < n; j++) {
			accu = 0;
			for (int k = 0; k < wsize; k++) {
				accu += data[*(tmp +k)*N + j];
			}
			if (accu > 0) { // ignore patches that are entirely background
				nonbackground++;
				ent[i] += table[accu];
			}
		}
		ent[i] = (nonbackground == 0) ? ((double) 0) : (ent[i] / ((double) nonbackground));
	}
}

//void multinomialent_s(double* ent, int* data, int* col_id, int* hist, int batch_len, int n, int N, int wsize, int base, double* table)
void multinomialent_s(double* ent, int* data, int* col_id, int batch_len, int n, int N, int base, double* table)
{
	int* tmpidx;
	int nonbackground;
	double entval = 2 * table[1];

	for (int i = 0; i < batch_len; i++) {
		ent[i] = 0;
		tmpidx = col_id + i * 2;
		nonbackground = 0;
		for (int j = 0; j < n; j++) {
			if (data[tmpidx[0]*N + j] + data[tmpidx[1]*N + j] > 0) {
				nonbackground++;
				double tmp = (data[tmpidx[0]*N + j] == data[tmpidx[1]*N + j]) ? 0 : entval;
				ent[i] += tmp;
			}
		}
		ent[i] = (nonbackground == 0) ? ((double) 0) : (ent[i] / ((double) nonbackground));
	}
}

// filename datafilename cooling printskip wsize outputfilename
int main(int argc, char **argv)
{
	int err = 0;
	char* ifname;
	char* ofname;
	int wsize = 2;						// window size
	int dim = 1;						// dimension of topology
	int distancetype = 0;				// tpye of distnace
	int sparse = 0;					// sparse features, 0 not sparse
	bool iflag = false;
	bool oflag = false;
	bool dflag = true;
	bool sflag = true;
	bool tflag = true;
	static char usage[] = "usage: %s [-dhst] -i ifname -o ofname\n";
	int ac = 1;	// argument count

	while (ac < argc) {
		if (!strcmp(argv[ac], "-h")) {
			printf(" \n");
			printf(usage, argv[0]);
			printf(" \n");
			printf("-i    input file name\n");
			printf("-o    output file name\n");
			printf("-d    dimension of topology, default value is 1\n");
			printf("-s    sparse features: 0 (default not sparse), 1 (sparse)\n");
			printf("-t    distance type: 0 Entropy (default), 1 Cityblock, 2 Euclidean\n");
			printf("-h    help\n");
			return 1;
		} else if (!strcmp(argv[ac], "-i")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ifname = argv[ac];
				iflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-o")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ofname = argv[ac];
				oflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-d")) {
			ac++;
			dflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				dim = atoi(argv[ac]);
				dflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-s")) {
			ac++;
			sflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				sparse = atoi(argv[ac]);
				sflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-t")) {
			ac++;
			tflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				distancetype = atoi(argv[ac]);
				tflag = true;
			}
			else {
				break;
			}
		} else {
			err = 1;
			break;
		}
		ac++;
	}

	if (!iflag) {
		fprintf(stderr, "%s: missing '-i ifname' option\n", argv[0]);
		return 0;
	} else if (!oflag) {
		fprintf(stderr, "%s: missing '-o ofname' option\n", argv[0]);
		return 0;
	} else if (!dflag) {
		fprintf(stderr, "%s: missing argument for '-d'\n", argv[0]);
		return 0;
	} else if (!sflag) {
		fprintf(stderr, "%s: missing argument for '-s'\n", argv[0]);
		return 0;
	} else if (!tflag) {
		fprintf(stderr, "%s: missing argument for '-t'\n", argv[0]);
		return 0;
	} else if (err == 1) {
		fprintf(stderr, usage, argv[0]);
		return 0;
	}

	printf("%s Starting...\n", argv[0]);
	printf("Input file:                          %s\n",ifname);
	printf("Output file:                         %s\n",ofname);
	printf("Topology dimension:                  %d\n", dim);
	printf("Sparse features:                     %d\n", sparse);

	FILE *ifile_ptr;
	ifile_ptr = fopen(ifname,"rb");
	if (ifile_ptr == NULL) {
		printf("Could not open input file %s.\n", ifname);
		return -1;
	}
	FILE *ofile_ptr;
	ofile_ptr = fopen(ofname,"wb");
	if (ofile_ptr == NULL) {
		printf("Could not open output file %s.\n", ofname);
		return -1;
	}

	int n, d, base;	// n sample size, d features, base number of values of each features
	int w, h;		// w width, h height, for 2d only
	if (dim == 1) {	// 1D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&base, sizeof(int), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}
	else {			// 2D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&w, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&h, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&base, sizeof(int), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}

	printf("Number of samples is                 %d\n", n);
	printf("Number of features is                %d\n", d);
	if (dim == 2) {	// 2D topology
		printf("Width of image is                    %d\n", w);
		printf("Height of image is                   %d\n", h);
	}
	printf("Number of discrete values is         %d\n", base);

	if (base > MAX_BASE) {
		printf("Number of discrete values in input file should not exceed %d.\n", MAX_BASE);
		return -1;
	}
	if (d > MAX_DIM) {
		printf("Feature dimension can not exceed %d.\n", MAX_DIM);
		return -1;
	}
	if ((dim < 1) || (dim > 2)) {
		printf("Topology dimension must be 1 or 2.\n");
		return -1;
	}
	if ((dim == 2) && (d != w*h)) {
		printf("dimension does not match width*height.\n");
		return -1;
	}
	if ((distancetype < 0) || (distancetype > 2)) {
		printf("Distancetype must be 0, 1, or 2.\n");
		return -1;
	}

	int option = 0;
	switch(base) {
	case 2 :
		option = CPU_BINARY_OTHER;
		break;
	default :
		option = CPU_NONBINARY_OTHER;
	}

	int* h_idx;		// features indices before swaps
	h_idx = (int *)malloc(d*sizeof(int));
	if (h_idx == NULL) {
		printf("Memory allocation for h_idx failed\n");
		return -1;
	}
	if (fread(h_idx, sizeof(int), d, ifile_ptr) != d) {
		printf("An error occurred reading feature indices.\n");
		return -1;
	}

	int* h_data;	// host data pointer
	int64_t nxy = n*d; // number of elements
	int64_t nBytes = nxy * sizeof(int);
	printf("Needs %lld Bytes for %lld elements.\n", nBytes, nxy);
	// allocate host memory
	h_data = (int *)calloc(nxy, sizeof(int));
	if (h_data == NULL) {
		printf("Memory allocation for h_data failed\n");
		return -1;
	}

	// column major
	for (int i = 0; i < d; i++) {
		if (fread(&h_data[i*n], sizeof(int), n, ifile_ptr) != n) {
			printf("Error reading data at i=%d.\n",i);
			return -1;
		}
	}
	fclose(ifile_ptr);
	if( (maxint(h_data, nxy) >= base) || (minint(h_data, nxy) < 0)) {
		printf("Input file discrete values should stay within 0 and %d, inclusive.\n", base-1);
		return -1;
	}

	int64_t num_neighbors = (int64_t) (d*(d-1)/2);	// number of distances
	double* ent_ori;								// entropy of original patches
	ent_ori = (double *)calloc(num_neighbors, sizeof(double));	// initialize to 0
	if (ent_ori == NULL) {
		printf("Memory allocation for ent_ori failed\n");
		return -1;
	}

	int col_id[2];								// grid ids of an affected
	double* ent_table;		// table to save numbers for entropy calculation
	ent_table = (double *)calloc((wsize+1), sizeof(double)); // initialize table
	for (int i = 1; i < wsize; i++) {
		ent_table[i] = -((double) i)/((double) wsize) * log((double)(i)/((double) wsize)) / log((double) base);
	}
	double* berent_table;		// table to save numbers for Bernoulli entropy calculation
	berent_table = (double *)calloc((wsize+1), sizeof(double));	// initialize table
	for (int i = 1; i < wsize; i++) {
		//berent_table[i] = ((double) (i * (wsize - i)))/((double) (wsize * wsize)); // Gini impurity
		double tmp = ((double) i)/((double) wsize);
		berent_table[i] = -(tmp * log(tmp) + (1.0-tmp) * log(1.0-tmp) )/ log(2.0);	// entropy
	}

	clock_t begin = clock();
	if (distancetype != 0) {
		metricdistance(ent_ori, h_data, d, n, distancetype);
	}
	else {
		if (sparse == 0) { // nonsparse features
			switch(option) {
				case CPU_BINARY_OTHER :
				for (int i = 0; i < d-1; i++) {
					for (int j = i+1; j < d; j++) {
						col_id[0] = i;
						col_id[1] = j;
						binogini(ent_ori + i * d - i * (i + 1)/2 + j - i - 1, h_data, col_id, 1, n, n, wsize, berent_table); // column major
					}
					printf(".");
				}
				break;
				case CPU_NONBINARY_OTHER :
				for (int i = 0; i < d-1; i++) {
					for (int j = i+1; j < d; j++) {
						col_id[0] = i;
						col_id[1] = j;
						multinomialent(ent_ori + i * d - i * (i + 1)/2 + j - i - 1, h_data, col_id, 1, n, n, base, ent_table); // column major
					}
					printf(".");
				}
			}
		}
		else { // sparse features
			switch(option) {
				case CPU_BINARY_OTHER :
				for (int i = 0; i < d-1; i++) {
					for (int j = i+1; j < d; j++) {
						col_id[0] = i;
						col_id[1] = j;
						binogini_s(ent_ori + i * d - i * (i + 1)/2 + j - i - 1, h_data, col_id, 1, n, n, wsize, berent_table); // column major
					}
					printf(".");
				}
				break;
				case CPU_NONBINARY_OTHER :
				for (int i = 0; i < d-1; i++) {
					for (int j = i+1; j < d; j++) {
						col_id[0] = i;
						col_id[1] = j;
						multinomialent_s(ent_ori + i * d - i * (i + 1)/2 + j - i - 1, h_data, col_id, 1, n, n, base, ent_table); // column major
					}
					printf(".");
				}
			}
		}
	}

	clock_t end = clock();
	double  elapsed = (double) (end - begin) * 1e3 / CLOCKS_PER_SEC; // in milliseconds
	printf("\nTime measured: %.0f milliseconds.\n", elapsed);

	double ENT_ori = 0;
	double ENT = 0;
	if (dim == 1) {
		int s,e;
		for (int i = 0; i < d-1; i++) {
			ENT_ori += ent_ori[i * d - i * (i + 1)/2 ];
			s = h_idx[i];
			e = h_idx[i+1];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
		}
		ENT_ori = (ENT_ori + ent_ori[d-2]) / ((double) d);
		s = h_idx[d-1];
		e = h_idx[0];
		if (s > e) {
			int tmp = s;
			s = e;
			e = tmp;
		}
		ENT = (ENT + ent_ori[s * d - (s * s + s)/2 + e - s - 1]) / ((double) d);
	}
	else {
		int row,col,s,e;
		for (int i = 0; i < d; i++) {
			row = i % h;	// find row index of i
			col = i / h;	// find col index of i
			s = i;
			e = ((col + 1) % w) * h + row;	// index of the horizontal neighbor of i
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT_ori += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = h_idx[i];
			e = h_idx[((col + 1) % w) * h + row];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = i;
			e = col * h + ((row + 1) % h);	// index of the vertical neighbor of i
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT_ori += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
			s = h_idx[i];
			e = h_idx[col * h + ((row + 1) % h)];
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			ENT += ent_ori[s * d - (s * s + s)/2 + e - s - 1];
		}
		ENT_ori = ENT_ori / (2.0 * ((double) d));
		ENT = ENT / (2.0 * ((double) d));
	}
	printf("ENT_ori = %.14f, ENT = %.14f\n", ENT_ori, ENT);

	// save new topology
	fwrite(&n, sizeof(int), 1, ofile_ptr);
	fwrite(&d, sizeof(int), 1, ofile_ptr);
	if (dim == 2) {
		fwrite(&w, sizeof(int), 1, ofile_ptr);
		fwrite(&h, sizeof(int), 1, ofile_ptr);
	}
	fwrite(&num_neighbors, sizeof(int64_t), 1, ofile_ptr);
	fwrite(h_idx, sizeof(int), d, ofile_ptr);
	fwrite(ent_ori, sizeof(double), num_neighbors, ofile_ptr);
	fclose(ofile_ptr);

	free(h_idx);
	free(h_data);
	free(ent_ori);
	free(ent_table);
	free(berent_table);
	return 0;
}
