Data Topology Learning (1D, 1.5D, and 2D topology)

Note: all binary data are save in little-endian format.

A. Convert raw data to an entropy table, run on CPU
- topDM.c (runs on cpu only, compiles with "cl -O2 topDM.c" on Windows 10, "gcc -std=c99 -O3 topDM.c -lm -o topDM" on Linux,
		   and "clang -std=c99 -O3 topDM.c -lm -o topDM" on MacOS)
	- usage: topDM [-dhst] -i inputfile -o outputfile
			-d				// dimension of topology, default is 1
			-s				// sparse features, 0 (default) not sparse, 1 sparse
			-t				// distance type: 0 Entropy (default), 1 Cityblock, 2 Euclidean
	- inputfile is a binary file (.bin) with the following format:
		- when topology dimension is 1 (i.e. "-d 1")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			base			// number of values of each features, 1 int32
			idx				// feature indices, i.e., initial feature topology, d int32
			data			// column major, n*d int32
		- when topology dimension is 2 (i.e. "-d 2")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			w				// width of 2d topology, 1 int32
			h				// height of 2d topology, 1 int32
			base			// number of values of each features, 1 int32
			idx				// feature indices, i.e., initial feature topology, d int32
			data			// column major, n*d int32
	- outputfile is a binary file (.dist) with the following format:
		- when topology dimension is 1 (i.e. "-d 1")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			num_neighbors	// number of feature pairs (no order), 1 int64
			idx				// feature indices, i.e., initial feature topology, d int32
			dist_tab		// paired distance table, num_neighbors double
		- when topology dimension is 2 (i.e. "-d 2")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			w				// width of 2d topology, 1 int32
			h				// height of 2d topology, 1 int32
			num_neighbors	// number of feature pairs (no order), 1 int64
			idx				// feature indices, i.e., initial feature topology, d int32
			dist_tab		// paired distance table, num_neighbors double

B. Convert raw data to an entropy table, run on GPU
- topDMGPU.cu (runs on gpu or cpu)
	- usage: topDMGPU [-bdghs] -i inputfile -o outputfile
			-b				// x dimension of a cuda block, needs to be a power of 2 no greater than 1024 (default 512)
			-d				// dimension of topology, default is 1
			-s				// sparse features, 0 (default) not sparse, 1 sparse
			-g				// 0 (use gpu when available, default), 1 (use cpu only)
	- inputfile is a binary file (.bin) with the following format:
		- when topology dimension is 1 (i.e. "-d 1")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			base			// number of values of each features, 1 int32
			idx				// feature indices, i.e., initial feature topology, d int32
			data			// column major, n*d int32
		- when topology dimension is 2 (i.e. "-d 2")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			w				// width of 2d topology, 1 int32
			h				// height of 2d topology, 1 int32
			base			// number of values of each features, 1 int32
			idx				// feature indices, i.e., initial feature topology, d int32
			data			// column major, n*d int32
	- outputfile is a binary file (.dist) with the following format:
		- when topology dimension is 1 (i.e. "-d 1")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			num_neighbors	// number of feature pairs (no order), 1 int64
			idx				// feature indices, i.e., initial feature topology, d int32
			dist_tab		// paired distance table, num_neighbors double
		- when topology dimension is 2 (i.e. "-d 2")
			n				// sample size, 1 int32
			d				// feature dimension, 1 int32
			w				// width of 2d topology, 1 int32
			h				// height of 2d topology, 1 int32
			num_neighbors	// number of feature pairs (no order), 1 int64
			idx				// feature indices, i.e., initial feature topology, d int32
			dist_tab		// paired distance table, num_neighbors double

C. multithreaded search for a minimum entropy topology, run on CPU
- topCPUT.c (runs on cpu only, compiles with "clang -std=gnu99 -O3 topCPUT.c -o topCPUT -lpthread" on MacOS,
 			"cc -std=gnu99 -O3 topCPUT.c -lm -o topCPUT -lpthread" on Linux and "cl -O2 topCPUT.c pthreadVC2.lib"
			on Windows 10 with pthread library installed). This code is portable on Windows 10, MacOS, and Linux.
			- usage: topCPUT [-cdehkmnpqrstuvx] -i inputfile -o outputfile
					-c              // number of cooling steps, default value is 50
					-d              // dimension of topology, default value is 1
					-e              // number of resets, default value is 0
					-f              // probability of accepting a worse state at the end, default value is 1e-8
					-k              // decay of initial temperature at each restart, (0,1], default value is 1.0
					-m              // file to save intermediate outputs, default is 'not saving to file'
					-n              // style of random initialization, 0 random (default), 0 < k < d random k nearest neighbor
					-p              // exit after printing out the estimated memory requirement, 0 or 1, default value is 0
					-q              // initial percentage of accepting a worse state, default value is 0.5
					-r              // random seed value, default is -1, i.e., time(NULL)
					-s              // number of threads, chosen from 1 to 128, default value is 1
					-t              // number of repeats for each temperature, default value is 5
					-u              // load an initial permutation from file, default value is null
					-v              // style of multithreaded search, 0 all threads start from the same state, each
					                //                                  thread restarts from its own minimum
					                //                                1 all threads start from the same state, each
					                //                                  thread restarts from the minimum of all threads
					                //                                2 each thread starts from a random state and
					                //                                  restarts from its own minimum
					                //                                3 each thread starts from a random state and
					                //                                  restarts from the minimum of all threads
					                //                                  default value is 0
					-x              // file name for indices of all cooling steps, default is 'not saving to file'
			- inputfile is a binary file (.dist) with the following format:
				- when topology dimension is 1 (i.e. "-d 1")
					n				// sample size, 1 int32
					d				// feature dimension, 1 int32
					num_neighbors	// number of feature pairs (unordered), 1 int64
					idx				// feature indices, i.e., initial feature topology, d int32
					dist_tab		// paired distance table, num_neighbors double
				- when topology dimension is 2 (i.e. "-d 2")
					n				// sample size, 1 int32
					d				// feature dimension, 1 int32
					w				// width of 2d topology, 1 int32
					h				// height of 2d topology, 1 int32
					num_neighbors	// number of feature pairs (no order), 1 int64
					idx				// feature indices, i.e., initial feature topology, d int32
					dist_tab		// paired distance table, num_neighbors double
			- outputfile is a binary file (.idx) with the following format:
					idx				// feature indices, i.e., learned feature topology, d int32
			- "-m intermediateoutputs" redirect contents shown on standard output to a text file "intermediateoutputs"
			- "-x allindices" save feature topology of each cooling step to a binary file "allindices":
					cooling			// number of cooling steps, 1 int32
					resets			// number of resets, 1 int32
					d				// feature dimension, 1 int32
					T				// temperature for coolingstep 0, 1 double
					ent				// entropy at the end of the coolingstep 0, 1 double
					idx0			// feature topology at the end of the coolingstep 0, d int32
					T				// temperature for coolingstep 1, 1 double
					ent				// entropy at the end of the coolingstep 1, 1 double
					idx1			// feature topology at the end of the coolingstep 1, d int32
					...
					T				// temperature for coolingstep cooling*(resets+1), 1 double
					ent				// entropy at the end of the coolingstep cooling*(resets+1), 1 double
					idxm			// feature topology at the end of the coolingstep cooling*(resets+1), d int32

D. 2-opt search
- RandomTour.c (runs on CPU, prepare input files for 2-opt search)
			- usage: RandomTour  <.dist file name> <dimension (1 or 2)> <population size>  <random seed>  <k random nearest neighbor>

- 2-Opt-clique.c (runs on CPU, 2-opt search)
			-usage:	2OPTC  <.dist file name> <window size>
			Note: window size >= 2. It needs RandomTour to generate all the input files. So run RandomTour first. It generates a .idx file with name identical to the input file.
			.idx file: d feature indices (int32) followed by corresponding feature entropies (double)

- 2-Opt-clique.cu (runs on CPU or GPU, 2-opt search)
			-usage: 2OPTG  <.dist file name> <window size> <filter style>
			Note: window size >= 2. filter style 0 (average), 1 (minimum), 2 (maximum). It generates a .idx file with name identical to the input file.
			.idx file: d feature indices (int32) followed by corresponding feature entropies (double)
			
E. Examples:
1. Generate an entropy table use CPU (1D topology), nonsparse features
topDM -i ..\Data\micky_shuffle0.bin -o ..\Data\micky_shuffle0.dist -t 0 -s 0

2. Generate an entropy table using GPU (1D topology), sparese features
topDMGPU -i ..\Data\micky_shuffle0.bin -o ..\Data\micky_shuffle0.dist -g 0 -s 1

3. Generate an entropy table using CPU (1D topology), nonsparse features
topDMGPU -i ..\Data\micky_shuffle0.bin -o ..\Data\micky_shuffle0.dist -g 1 -s 0

4. Search for a minimum entropy 1D topology without restarts
topCPU -i ..\Data\micky_shuffle0.dist -o ..\Data\micky_base16_10000x40r0.idx -c 10000 -r 0 -t 40 -e 1 -s 4 -v 1
Description of the input arguments:
"-i  ..\Data\micky_shuffle0.dist"			load the entropy table from this binary file
"-o ..\Data\micky_base16_10000x40r0.idx"	save the final topology to this binary file
"-c 10000"									10000 cooling temperatures
"-t 40"										at each cooling temperature, each possible swap is visited 40 times in a random order
"-r 0"										rand seed is 0
"-e 1"                                      restart 1 time, i.e., after the first 10000 cooling temperatures, restart optimization from the current all-thread minima
"-s 4                                       4 threads
"-v 1"										multithreaded search style 1

5. Search for a minimum entropy 1D topology with restarts
topCPU -i ..\Data\micky_shuffle0.dist -o ..\Data\micky_base16_1000x40r0.idx -u ..\Data\micky_base16_10000x40r0.idx -c 1000 -t 40 -e 1 -r 0
Description of the input arguments:
"-i  ..\Data\micky_shuffle0.dist"			load the entropy table from this binary file
"-o ..\Data\micky_base16_1000x40r0.idx"		save the final topology to this binary file
"-u ..\Data\micky_base16_10000x40r0.idx"	load the initial topology from this binary file
"-c 1000"									1000 cooling temperatures
"-t 40"										at each cooling temperature, each possible swap is visited 40 times in a random order
"-e 1"										restart 1 time, i.e., after the first 1000 cooling temperatures, restart optimization from the current minima
"-r 0"										rand seed is 0

6. Search for a minimum entropy 1D topology with restarts (noninteractive running on a cluster)
topCPU -i ..\Data\micky_shuffle0.dist -o ..\Data\micky_base16_10000x40r0.idx -m ..\Output\micky_base16_10000x40r0.out -c 1000 -r 0 -t 40 -v 1 -e 1 -n 3
"-i  ..\Data\micky_shuffle0.dist"			load the entropy table from this binary file
"-o ..\Data\micky_base16_10000x40r0.idx"	save the final topology to this binary file
"-m ..\Output\micky_base16_10000x40r0.out"	save the output after each cooling temperature in this text file (when running on a cluster, check this file for progress)
"-c 1000"									1000 cooling temperatures
"-t 40"										at each cooling temperature, each possible swap is visited 40 times in a random order
"-r 0"										rand seed is 0
"-e 1"										restart 1 time, i.e., after the first 1000 cooling temperatures, restart optimization from the current minima
"-n 3"										using random 3 nearest neighbor initializaation

7. Generate an entropy table (2D topology)
topDMGPU -i ..\Data\mnist\mnist60000.bin -o ..\Data\mnist\mnist.dist -d 2 -g 0
"-d 2"										2D topology
"-g 0"										use GPU

8. Search for a minimun entropy 2D topology
topCPU -i ..\Data\mnist\mnist.dist -o ..\Data\mnist\mnist20000x50w2r1p65e0.idx -d 2 -c 20000 -t 50 -r 1 -q 65 -v 1-s 4 -e 0
"-i ..\Data\mnist\mnist.dist"				load the entropy table from this binary file
"-o ..\Data\mnist\mnist20000x50w2r1p65e0.idx"	save the final topology to this binary file
"-d 2"										2D topology
"-c 20000"									20000 cooling temperatures
"-t 50"										at each cooling temperature, each possible swap is visited 50 times in a random order
"-r 1"										random seed is 1
"-s 4"										use 4 threads
"-v 2"										multithreaded search style 2
"-q 65"										starting probability of accepting a worse state is 0.65
"-e 0"										no restart

9. Search for a minimum entorpy 1.5D topology via two 1D searches (find row and col permutations of 2D images)
topDMGPU -i ..\Data\mnist\mnistcol.bin -o ..\Data\mnist\mnistcol.dist
topDMGPU -i ..\Data\mnist\mnistrow.bin -o ..\Data\mnist\mnistrow.dist
topCPU -i ..\Data\mnist\mnistcol.dist -o ..\Data\mnist\colperm.idx -c 100 -r 0 -t 10
topCPU -i ..\Data\mnist\mnistrow.dist -o ..\Data\mnist\rolperm.idx -c 100 -r 0 -t 10

10. multithreaded search, 1D topology, 16 threads
topCPUT -i ..\Data\micky_shuffle0.dist -o ..\Data\micky_base16_1000x20r0.idx -m ..\Output\micky_base16_1000x20r0.out -c 1000 -t 20 -p 0.5 -f 0.001 -e 1 -r 0 -v 2 -s 16
"-i  ..\Data\micky_shuffle0.dist"			load the entropy table from this binary file
"-o ..\Data\micky_base16_1000x20r0.idx"		save the final topology to this binary file
"-m ..\Output\micky_base16_1000x20r0.out"	save the output after each cooling temperature in this text file (when running on a cluster, check this file for progress)
"-c 1000"									1000 cooling temperatures
"-t 20"										at each cooling temperature, each possible swap is visited 20 times in a random order
"-q 0.5"									accepting probability at the start of the search
"-f 0.001"									accepting probability at the end of the search
"-e 1"										restart 1 time, i.e., after the first 1000 cooling temperatures, restart optimization from the current minima
"-r 0"										rand seed is 0
"-v 2"										multithreaded search style 2, i.e. all threads are independent
"-s 16"										16 threads

11. 2-opt search, dense features, CPU
topDM -i micky_shuffle0.bin -o micky_shuffle0.dist				// generate the distance matrix, dense features, run on CPU
RandomTour micky_shuffle0.dist 1 20 0 3							// random seed 0, 20 random initial starts using random 3 nearest neighbor
2OPTC micky_shuffle0.dist 2											// window size 2, run on CPU

12. 2-opt search, sparse features, CPU
topDMGPU -i micky_shuffle0.bin -o micky_shuffle0.dist -g 0 -s 1	// generate the distance matrix, sparse features, run on GPU
RandomTour micky_shuffle0.dist 1 20 0 3							// random seed 0, 20 random initial starts using random 3 nearest neighbor
2OPTC micky_shuffle0.dist 2										// window size 2, run on CPU

13. 2-opt search, sparse features, max filtering, GPU
topDMGPU -i micky_shuffle0.bin -o micky_shuffle0.dist -g 0 -s 1	// generate the distance matrix, sparse features, run on GPU
RandomTour micky_shuffle0.dist 1 20 0 3							// random seed 0, 20 random initial starts using random 3 nearest neighbor
2OPTG micky_shuffle0.dist 3 2									// window size 3, run on GPU, window max

14. 2-opt search, sparse features, average filtering, GPU
topDMGPU -i micky_shuffle0.bin -o micky_shuffle0.dist -g 0 -s 1	// generate the distance matrix, sparse features, run on GPU
RandomTour micky_shuffle0.dist 1 20 0 3							// random seed 0, 20 random initial starts using random 3 nearest neighbor
2OPTG micky_shuffle0.dist 5 0									// window size 5, run on GPU, window average
