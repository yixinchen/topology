/* portable */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#include <math.h>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

const uint64_t RAND_MAX_PORTABLE = 2147483647;
const uint64_t URANDINT_MAX = 0x3FFFFFFFFFFFFFFF;
const double EPS = 2.220446049250313e-16;
const int MAX_DIM = 65535;				// maximum input dimension
const double MAX_DB = 1.7e+308;		// maximum double

// find index of minimum element in data
int mindouble(double* data, int len)
{
	int minptr = 0;
	for(int i = 1; i < len; i++) {
		minptr = data[minptr] > data[i] ? i : minptr;
	}
	return minptr;
}

// find index of maximum element in data
int maxdouble(double* data, int len)
{
	int maxptr = 0;
	for(int i = 1; i < len; i++) {
		maxptr = data[maxptr] < data[i] ? i : maxptr;
	}
	return maxptr;
}

// find windows affected by a swap, return the number and indices of affected windows
int findaffected1d(int* affected, int s, int e, int d)
{
	int cnt = 2;
	affected[0] = s;
	affected[1] = (s == 0) ? d - 1 : s - 1;
	affected[2] = e;
	affected[3] = (e == 0) ? d - 1 : e - 1;
	for (int i = 2; i < 4; i++) {
		if ((affected[i] != affected[0]) &&
			(affected[i] != affected[1])) {
				affected[cnt] = affected[i];
				cnt++;
			}
	}
	return cnt;
}

// find 1d indices of L-shaped windows affected by a swap, return the number and indices
// of affected windows; window has size 3 covering grids (x,y), (x,y+1), and (x+1,y)
int findaffected2d(int* affected, int s, int e, int w, int h)
{
	int rs = s % h; // row index of pixel s
	int cs = s / h; // column index of pixel s
	int re = e % h; // row index of pixel e
	int ce = e / h; // column index of pixel e
	int cnt = 3;

	affected[0] = s;
	affected[1] = (rs == 0) ? s + h - 1 : s - 1; // grid above
	affected[2] = (cs == 0) ? (w - 1) * h + rs : s - h; // grid on the left
	affected[3] = e;
	affected[4] = (re == 0) ? e + h - 1 : e - 1; // grid above
	affected[5] = (ce == 0) ? (w - 1) * h + re : e - h; // grid on the left

	for (int i = 3; i < 6; i++) {
		if ((affected[i] != affected[0]) &&
			(affected[i] != affected[1]) &&
			(affected[i] != affected[2])) {
				affected[cnt] = affected[i];
				cnt++;
			}
	}
	return cnt;
}

double mean(double* x, int d)
{
	double accu = 0.0;
	for (int i = 0; i < d; i++) {
		accu += x[i];
	}
	return accu/((double) d);
}

static int mdo_rand(uint64_t *ctx)
{
/*
 * Compute x = (7^5 * x) mod (2^31 - 1)
 * without overflowing 31 bits:
 *      (2^31 - 1) = 127773 * (7^5) + 2836
 * From "Random number generators: good ones are hard to find",
 * Park and Miller, Communications of the ACM, vol. 31, no. 10,
 * October 1988, p. 1195.
 */
	int64_t hi, lo, x;

	/* Can't be initialized with 0, so use another value. */
	if (*ctx == 0)
		*ctx = 123459876;
	hi = *ctx / 127773;
	lo = *ctx % 127773;
	x = 16807 * lo - 2836 * hi;
	if (x < 0)
		x += 0x7fffffff;
	return ((*ctx = x) % ((uint64_t)RAND_MAX_PORTABLE + 1));
}

int mrand_r(uint32_t *ctx)
{
	uint64_t val = (uint64_t) *ctx;
	int r = mdo_rand(&val);

	*ctx = (uint32_t) val;
	return (r);
}

static uint64_t next = 1;

int mrand()
{
	return (mdo_rand(&next));
}

void msrand(uint32_t seed)
{
	next = seed;
}

// uniform random number between 0 and N inclusive
int randomU(int N, uint32_t* seed)
{
	return mrand_r(seed) % (N+1);
}

// uniform random number between 0 and 1
double randu(uint32_t* seed)
{
	uint64_t r = mrand_r(seed) & 0x7FFFFFFF;
	r = r << 31 | (mrand_r(seed) & 0x7FFFFFFF);
	return ((double) r)/((double) URANDINT_MAX);
}

// random permutation of idx of size len, Durstenfeld's algorithm
void randperm(int* idx, int len, uint32_t* seed)
{
	int j;
	int junk;
	for (int i = len - 1; i > 0; i--) {
		//j = randomU(i, seed);
		j = mrand_r(seed) % (i+1);
		junk = idx[i];
		idx[i] = idx[j];
		idx[j] = junk;
	}
}

// random k nearest neighbor, randomly select from the k nearest neighbors
void randnn(int* idx, int len, uint32_t* seed, double* dist_tab, int k)
{
	int ss, ee;
	int* scheduled;
	double* kmin;		// pool of current k minimums
	int* kmin_idx;	// indices of the elements in the kmin pool
	scheduled = (int *)calloc(len, sizeof(int));
	kmin = (double *)calloc(k, sizeof(double));
	kmin_idx = (int *)calloc(k, sizeof(int));

	idx[0] = mrand_r(seed) % len;
	scheduled[idx[0]] = 1;
	for (int i = 0; i < len - 1; i++) {
		for (int j = 0; j < k; j++) {	// initialize k-NN pool
			kmin[j] = MAX_DB;
			kmin_idx[j] = 0;
		}
		double maxval = MAX_DB;	// max value of kmin pool
		int max_idx = 0;	// index of the maxval
		// number of updates to kmin pool, the size of the kmin pool gets smaller in the last k-1 iterations
		int numupdates = 0;
		for (int j = 0; j < len; j++) {
			if ((j != idx[i]) && (scheduled[j] == 0)) {
				if (idx[i] > j) {
					ss = j;
					ee = idx[i];
				} else {
					ss = idx[i];
					ee = j;
				}
				double current_dist = dist_tab[ss * len - (ss * ss + ss)/2 + ee - ss - 1];
				if (current_dist <= maxval) { // update k-min pool
					kmin[max_idx] = current_dist;
					kmin_idx[max_idx] = j;
					max_idx = maxdouble(kmin, k);
					maxval = kmin[max_idx];
					numupdates++;
				}
			}
		}
		int kminpoolsize = (numupdates < k) ? numupdates : k;
		idx[i+1] = kmin_idx[mrand_r(seed) % kminpoolsize];
		scheduled[idx[i+1]] = 1;
	}
	free(scheduled);
	free(kmin);
	free(kmin_idx);
}

typedef struct {
	int dim, d, w, h;
	int64_t repeatbd, num_neighbors;
	double epsilon;
	int* pairs;
	double* dist_tab;
	double* T;
	int* h_idx;
	int* h_idx_out;
	int* idx_saved;
	int* stateidx;
	int* affected;
	int64_t* a_cnt;
	double* ent;
	double* ent_saved;
	double* affected_ent;
	double* dEave;
	double* ENT_saved;
	unsigned int* seed;
} optimizer_struct;

void *optimize(void* args)
{
	optimizer_struct* actual_args;
	actual_args = args;
	int dim = actual_args -> dim;
	int d = actual_args -> d;
	int w = actual_args -> w;
	int h = actual_args -> h;
	int64_t repeatbd = actual_args -> repeatbd;
	int64_t num_neighbors = actual_args -> num_neighbors;
	double epsilon = actual_args -> epsilon;
	int* pairs = actual_args -> pairs;
	double* dist_tab = actual_args -> dist_tab;
	double T = *(actual_args -> T);
	int* h_idx = actual_args -> h_idx;
	int* h_idx_out = actual_args -> h_idx_out;
	int* idx_saved = actual_args -> idx_saved;
	int* stateidx = actual_args -> stateidx;
	int* affected = actual_args -> affected;
	int64_t* a_cnt = actual_args -> a_cnt;
	double* ent = actual_args -> ent;
	double* ent_saved = actual_args -> ent_saved;
	double* affected_ent = actual_args -> affected_ent;
	double* dEave = actual_args -> dEave;
	double* ENT_saved = actual_args -> ENT_saved;
	unsigned int* seed = actual_args -> seed;

	int64_t repeatcnt = 0;			// cnt for number of interations for a given temperature
	int s,e;
	int64_t j = 0;
	int num_affected;
	int64_t stuck = 0;
	double p, P;
	double delta;
	double savedent;

	while (repeatcnt < repeatbd) {
		repeatcnt++;						// number of pairs visited for the current temperature
		s = pairs[2 * stateidx[j]];			// features to be swapped
		e = pairs[2 * stateidx[j] + 1];		// features to be swapped
		j = (j + 1) % num_neighbors;		// index of the next pair to check
		// find the windows affected by the swap and the number of affected windows
		if (dim == 1) {
			num_affected = findaffected1d(affected, s, e, d);
		}
		else {
			num_affected = findaffected2d(affected, s, e, w, h);
		}
		h_idx_out[s] = h_idx[e];			// make the swap (temporary)
		h_idx_out[e] = h_idx[s];

		if (dim == 1) {
			for (int i = 0; i < num_affected; i++) {
				// find the grid ids of a window
				int a = affected[i];
				int b = (a + 1) % d;
				// find the feature ids
				int ss = h_idx_out[a];
				int ee = h_idx_out[b];
				if (ss > ee) {
					int tmp = ss;
					ss = ee;
					ee = tmp;
				}
				affected_ent[i] = dist_tab[ss * d - (ss * ss + ss)/2 + ee - ss - 1];	// affected entropy
			}
		}
		else {
			for (int i = 0; i < num_affected; i++) {
				// find the grid ids of a window
				int row = affected[i] % h;	// row index of affected[i]
				int col = affected[i] / h;	// col index of affected[i]
				int a = affected[i];
				int b = ((col + 1) % w) * h + row;	// horizontal neighbor of i
				int ss = h_idx_out[a];
				int ee = h_idx_out[b];
				if (ss > ee) {
					int tmp = ss;
					ss = ee;
					ee = tmp;
				}
				affected_ent[i] = dist_tab[ss * d - (ss * ss + ss)/2 + ee - ss - 1];	// affected entropy of horizontal window
				b =  col * h + ((row + 1) % h);	// vertical neighbor of i
				ss = h_idx_out[a];
				ee = h_idx_out[b];
				if (ss > ee) {
					int tmp = ss;
					ss = ee;
					ee = tmp;
				}
				affected_ent[i] += dist_tab[ss * d - (ss * ss + ss)/2 + ee - ss - 1];	// affected entropy of vertical window
				affected_ent[i] /= 2.0;
			}
		}

		// calculate change of entropy, delta
		delta = 0;
		for (int i = 0; i < num_affected; i++) {
			delta += affected_ent[i] - ent[affected[i]];
		}

		// avoiding that the first state is worse, then P is evaluated to 0, hence trapped there
		if ((delta > 0) && (dEave[0] == 0.0)) {
			dEave[0] = delta;
		}
		// if the objective function value of the random state is better than that of the original
		double eps = delta + epsilon * ((double) num_affected);
		if (eps < 0) {
			a_cnt[0]++;
			// running average of the accepted change of objective function value
			dEave[0] = (dEave[0] * (double)(a_cnt[0] -1) - delta)/((double)a_cnt[0]);
			for (int k = 0; k < num_affected; k++) {
				ent[affected[k]] = affected_ent[k];
			}
			h_idx[e] = h_idx_out[e];	// make the swap (permanant)
			h_idx[s] = h_idx_out[s];
			stuck = 0;	// no longer at a local optimum
		}
		else { // if the state is worse
			stuck++;
			// stuck for more than num_neighbors-1 consecutive pairs
			if (stuck == 1) {
				// save the average objective function value of the local minimum
				savedent = mean(ent, d);
				if (ENT_saved[0] > savedent) {
					ENT_saved[0] = savedent;
					for (int k = 0; k < d; k++) {
						idx_saved[k] = h_idx[k];
						ent_saved[k] = ent[k];
					}
				}
			}
			p = randu(seed);
			P = exp((-eps)/(dEave[0] * T));	// estimate Boltzmann probability
			if (p < P) { // accept a worse state
				for (int k = 0; k < num_affected; k++) {	// update window entropies
					ent[affected[k]] = affected_ent[k];
				}
				h_idx[e] = h_idx_out[e];	// make the swap (permanant)
				h_idx[s] = h_idx_out[s];
			}
			else {
				h_idx_out[e] = h_idx[e];	// restore feature ids to the previous value
				h_idx_out[s] = h_idx[s];
			}
		}
	}
	randperm(stateidx, (int) num_neighbors, seed);	// generate a new order

	return NULL;
}

void entropy(double* ent, double* dist_tab, int* idx, int dim, int d, int w, int h)
{
	int a, b;		// indices for two grids in a window
	if (dim == 1) {	// 1D topology
		for (int i = 0; i < d-1; i++) {
			a = idx[i];
			b = idx[i+1];
			if (a > b) {
				int tmp = a;
				a = b;
				b = tmp;
			}
			ent[i] = dist_tab[a * d - (a * a + a)/2 + b - a - 1];
		}
		a = idx[d-1];
		b = idx[0];
		if (a > b) {
			int tmp = a;
			a = b;
			b = tmp;
		}
		ent[d-1] = dist_tab[a * d - (a * a + a)/2 + b - a - 1];
	}
	else {	// 2D topology
		int row,col;
		for (int i = 0; i < d; i++) {
			row = i % h;	// find row index of i
			col = i / h;	// find col index of i
			a = idx[i];
			b = idx[((col + 1) % w) * h + row];
			if (a > b) {
				int tmp = a;
				a = b;
				b = tmp;
			}
			ent[i] = dist_tab[a * d - (a * a + a)/2 + b - a - 1];
			a = idx[i];
			b = idx[col * h + ((row + 1) % h)];
			if (a > b) {
				int tmp = a;
				a = b;
				b = tmp;
			}
			ent[i] += dist_tab[a * d - (a * a + a)/2 + b - a - 1];
			ent[i] /= 2.0;
		}
	}
}

int main(int argc, char **argv)
{
	int err = 0;
	char* ifname;
	char* ofname;
	char* mfname = NULL;				// file containing temporary results
	char* pfname = NULL;				// file containing initial permutation
	char* xfname = NULL;				// file containing indices from each cooling steps
	int cooling = 100;	 				// temperature cooling steps
	int wsize = 2;						  // window size
	int printmem = 0;				    // return after print the estimated memory requirement
	int numthreads = 1; 				// number of threads
	int repeat = 5;						  // number of repeats for each temperature
	int resets = 0;						  // number of resets
	int dim = 1;						    // dimension of topology
	int style = 0;						  // style of multithreaded search
	int rnn = 0;							  // random nearest neighbor initialization
	double Ps = 0.5;            // probability of accepting a worse state at the start
	double Pf = 1e-8;           // probability of accepting a worse state at the end
	double decay = 1.0;         // decay of intial temperature at each restart
	bool iflag = false;
	bool oflag = false;
	bool mflag = true;
	bool nflag = true;
	bool rflag = true;
	bool cflag = true;
	bool dflag = true;
	bool eflag = true;
	bool fflag = true;
	bool kflag = true;
	bool pflag = true;
	bool qflag = true;
	bool sflag = true;
	bool tflag = true;
	bool vflag = true;
	bool uflag = true;
	bool xflag = true;
	int rseed = -1;
	static char usage[] = "usage: %s [-cdefhkmnpqrstuvx] -i ifname -o ofname\n";
	int ac = 1;	// argument count

	while (ac < argc) {
		if (!strcmp(argv[ac], "-h")) {
			printf(" \n");
			printf(usage, argv[0]);
			printf(" \n");
			printf("-i      input file name\n");
			printf("-o      output file name\n");
			printf("-c      number of cooling steps, default value is 100\n");
			printf("-d      dimension of topology, 1 or 2, default value is 1\n");
			printf("-e      number of resets, default value is 0\n");
			printf("-f      probability of accepting a worse state at the end, default value is 1e-8\n");
			printf("-h      help\n");
			printf("-k      decay of initial temperature at each restart, (0,1], default value is 1.0\n");
			printf("-m      file to save intermediate outputs, default is 'not saving to file'\n");
			printf("-n      style of random initialization, 0 random (default), k > 0 random k nearest neighbor\n");
			printf("-p      return after printing out the estimated memory requirement, 0 or 1, default value is 0\n");
			printf("-q      probability of accepting a worse state at the start, default value is 0.5\n");
			printf("-r      random seed value, default is -1, i.e., time(NULL)\n");
			printf("-s      number of threads, chosen from 1 to 128, default value is 1\n");
			printf("-t      number of repeats for each temperature, default value is 5\n");
			printf("-u      load an initial permutation from file, default value is null\n");
			printf("-v      style of multithreaded search, 0 all threads start from the same state, each thread\n");
			printf("                                         restarts from its own minimum\n");
			printf("                                       1 all threads start from the same state, each thread\n");
			printf("                                         restarts from the minimum of all threads\n");
			printf("                                       2 each thread starts from a random state and restarts\n");
			printf("                                         from its own minimum\n");
			printf("                                       3 each thread starts from a random state and restarts\n");
			printf("                                         from the minimum of all threads\n");
			printf("                                       default value is 0\n");
			printf("-x      file name for indices of all cooling steps, default is 'not saving to file'\n");
			return 1;
		} else if (!strcmp(argv[ac], "-i")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ifname = argv[ac];
				iflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-o")) {
			ac++;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				ofname = argv[ac];
				oflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-m")) {
			ac++;
			mflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				mfname = argv[ac];
				mflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-u")) {
			ac++;
			uflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				pfname = argv[ac];
				uflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-x")) {
			ac++;
			xflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				xfname = argv[ac];
				xflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-d")) {
			ac++;
			dflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				dim = atoi(argv[ac]);
				dflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-c")) {
			ac++;
			cflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				cooling = atoi(argv[ac]);
				cflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-e")) {
			ac++;
			eflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				resets = atoi(argv[ac]);
				eflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-p")) {
			ac++;
			pflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				printmem = atoi(argv[ac]) > 0 ? 1 : 0;
				pflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-q")) {
			ac++;
			qflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				Ps = atof(argv[ac]);
				qflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-f")) {
			ac++;
			fflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				Pf = atof(argv[ac]);
				fflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-k")) {
			ac++;
			kflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				decay = atof(argv[ac]);
				kflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-r")) {
			ac++;
			rflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				rseed = abs(atoi(argv[ac]));
				rflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-s")) {
			ac++;
			sflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				numthreads = atoi(argv[ac]);
				sflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-t")) {
			ac++;
			tflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				repeat = atoi(argv[ac]);
				tflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-v")) {
			ac++;
			vflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				style = atoi(argv[ac]);
				vflag = true;
			}
			else {
				break;
			}
		} else if (!strcmp(argv[ac], "-n")) {
			ac++;
			nflag = false;
			if (ac >= argc) {
				break;
			}
			if (argv[ac][0] != '-') {
				rnn = atoi(argv[ac]);
				nflag = true;
			}
			else {
				break;
			}
		} else {
			err = 1;
			break;
		}
		ac++;
	}

	if (!iflag) {
		fprintf(stderr, "%s: missing '-i ifname' option\n", argv[0]);
		return 0;
	} else if (!oflag) {
		fprintf(stderr, "%s: missing '-o ofname' option\n", argv[0]);
		return 0;
	} else if (!cflag) {
		fprintf(stderr, "%s: missing argument for '-c'\n", argv[0]);
		return 0;
	} else if (!dflag) {
		fprintf(stderr, "%s: missing argument for '-d'\n", argv[0]);
		return 0;
	} else if (!eflag) {
		fprintf(stderr, "%s: missing argument for '-e'\n", argv[0]);
		return 0;
	} else if (!fflag) {
		fprintf(stderr, "%s: missing argument for '-f'\n", argv[0]);
		return 0;
	} else if (!mflag) {
		fprintf(stderr, "%s: missing argument for '-m'\n", argv[0]);
		return 0;
	} else if (!nflag) {
		fprintf(stderr, "%s: missing argument for '-n'\n", argv[0]);
		return 0;
	} else if (!pflag) {
		fprintf(stderr, "%s: missing argument for '-p'\n", argv[0]);
		return 0;
	} else if (!qflag) {
		fprintf(stderr, "%s: missing argument for '-q'\n", argv[0]);
		return 0;
	} else if (!kflag) {
		fprintf(stderr, "%s: missing argument for '-k'\n", argv[0]);
		return 0;
	} else if (!rflag) {
		fprintf(stderr, "%s: missing argument for '-r'\n", argv[0]);
		return 0;
	} else if (!sflag) {
		fprintf(stderr, "%s: missing argument for '-s'\n", argv[0]);
		return 0;
	} else if (!tflag) {
		fprintf(stderr, "%s: missing argument for '-t'\n", argv[0]);
		return 0;
	} else if (!vflag) {
		fprintf(stderr, "%s: missing argument for '-v'\n", argv[0]);
		return 0;
	} else if (!uflag) {
		fprintf(stderr, "%s: missing argument for '-u'\n", argv[0]);
		return 0;
	} else if (!xflag) {
		fprintf(stderr, "%s: missing argument for '-x'\n", argv[0]);
		return 0;
	} else if (err == 1) {
		fprintf(stderr, usage, argv[0]);
		return 0;
	}

	if (dim == 2) {
		wsize = 3;
	}

	if (rseed >= 0) {
		msrand(rseed);								// Initialized random number generator
	} else {
		printf("random seed\n");
		msrand(time(NULL));
	}

	printf("%s Starting...\n", argv[0]);
	printf("Input file:                          %s\n",ifname);
	printf("Output file:                         %s\n",ofname);
	printf("Topology dimension:                  %d\n", dim);
	if (mfname != NULL) {
		printf("Temporary file:                      %s\n",mfname);
	}
	if (pfname != NULL) {
		printf("Initial permutation file:            %s\n",pfname);
	}
	if (xfname != NULL) {
		printf("Indices of all cooling steps file:   %s\n",xfname);
	}
	printf("Number of cooling iterations:        %d\n", cooling);
	printf("Repeat:                              %d\n", repeat);
	printf("Decay:                               %.2f\n", decay);
	printf("Resets:                              %d\n", resets);
	printf("Window size:                         %d\n", wsize);
	printf("Initial accepting probability:       %.5E\n", Ps);
	printf("Final accepting probability:         %.5E\n", Pf);
	printf("Random seed:                         %d\n", rseed);
	printf("Number of threads:                   %d\n", numthreads);
	printf("Style of multithreaded search:       %d\n", style);
	printf("Random k-NN initialization:          %d\n", rnn);

	FILE *ifile_ptr;
	ifile_ptr = fopen(ifname,"rb");
	if (ifile_ptr == NULL) {
		printf("Could not open input file %s.\n", ifname);
		return -1;
	}
	FILE *ofile_ptr;
	ofile_ptr = fopen(ofname,"wb");
	if (ofile_ptr == NULL) {
		printf("Could not open input file %s.\n", ofname);
		return -1;
	}

	int d;	// d features
	int n;	// sample size, need it to define epsilon
	int w, h;		// w width, h height, for 2d only
	int64_t num_neighbors;	// num_neighbors = d * (d - 1) / 2
	if (dim == 1) {	// 1D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&num_neighbors, sizeof(int64_t), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}
	else {			// 2D topology
		if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&w, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&h, sizeof(int), 1, ifile_ptr) != 1) ||
			(fread(&num_neighbors, sizeof(int64_t), 1, ifile_ptr) != 1)) {
			printf("An error occurred reading the dimension values.\n");
			return -1;
		}
	}
	if( num_neighbors != (int64_t) (d * (d - 1) / 2)) {
		printf("Feature dimension does not match the size of distance matrix.\n");
		return -1;
	}
	printf("Number of samples is                 %d\n", n);
	printf("Number of features is                %d\n", d);
	if (dim == 2) {	// 2D topology
		printf("Width of image is                    %d\n", w);
		printf("Height of image is                   %d\n", h);
	}

	if (numthreads < 1 || numthreads > 128) {
		printf("number of threads should be choosen from 1 to 128\n");
		return -1;
	}
	if ((Ps > 1) || (Pf > 1) || (Ps <= Pf)) {
		printf("Initial and final accepting probability should satisfy 0 < Pf < Ps < 1\n");
		return -1;
	}
	if (decay > 1.0) {
		printf("Decay cannot exceed 1.0\n");
		return -1;
	}
	if (d > MAX_DIM) {
		printf("Feature dimension can not exceed %d.\n", MAX_DIM);
		return -1;
	}
	if (repeat == 0) {
		printf("repeat should be greater than 0.\n");
	}
	if ((dim < 1) || (dim > 2)) {
		printf("Topology dimension must be 1 or 2.\n");
		return -1;
	}
	if ((dim == 2) && (d != w*h)) {
		printf("dimension does not match width*height.\n");
		return -1;
	}
	if ((style < 0) || (style > 3)) {
		printf("style should be 0, 1, 2, or 3");
		return -1;
	}
	if ((rnn < 0) || (rnn >= d)) {
		printf("random nearest neighbor initialization must be between 0 and d-1");
		return -1;
	}

	// calculated memory requirement
	int64_t memsize;
	int GB = 1<<30;
	memsize = 16 * num_neighbors + 8 * (cooling * (resets + 1) + 1) + wsize * 8 + \
			  numthreads * (28 * d + 4 * num_neighbors + sizeof(optimizer_struct) + 24 * wsize + 72) + 1024 * 1024;
	int est_mem_gb = (memsize + 1) / GB + 1;
	printf("Recommended memory requirment: %d GB\n", est_mem_gb);
	if (printmem == 1) {
		fclose(ifile_ptr);
		fclose(ofile_ptr);
		return 0;
	}

	int** h_idx;		// features indices before swaps
	int** h_idx_out;	// feature indices after swaps
	int** idx_saved; // current best feature indices
	h_idx = (int **)malloc(numthreads * sizeof(int*));
	h_idx_out = (int **)malloc(numthreads * sizeof(int*));
	idx_saved = (int **)malloc(numthreads * sizeof(int*));
	if ((h_idx == NULL) || (h_idx_out == NULL) || (idx_saved == NULL)) {
		printf("Memory allocation for h_idx or h_idx_out or idx_saved failed\n");
		return -1;
	}
	for (int i = 0; i < numthreads; i++) {
		h_idx[i] = (int *)malloc(d * sizeof(int));
		h_idx_out[i] = (int *)malloc(d * sizeof(int));
		idx_saved[i] = (int *)malloc(d * sizeof(int));
		if ((h_idx[i] == NULL) || (h_idx_out[i] == NULL) || (idx_saved[i] == NULL)) {
			printf("Memory allocation for h_idx[%d] or h_idx_out[%d] or idx_saved[%d] failed\n", i, i, i);
		}
	}
	if (fread(h_idx[0], sizeof(int), d, ifile_ptr) != d) {
		printf("An error occurred reading feature indices.\n");
		return -1;
	}
	double* dist_tab;	// distance table pointer
	int64_t NBytes = num_neighbors * sizeof(double);
	printf("Needs %lld Bytes for %lld distances.\n", NBytes, num_neighbors);
	// allocate memory for the distance table
	dist_tab = (double *)malloc(NBytes);
	if (dist_tab == NULL) {
		printf("Memory allocation for dist_tab failed\n");
		return -1;
	}
	printf("Loading data ...... ");
	if (fread(dist_tab, sizeof(double), num_neighbors, ifile_ptr) != num_neighbors) {
		printf("Error reading dist_tab.\n");
		return -1;
	}
	fclose(ifile_ptr);
	printf(" Finished\n");

	uint32_t* tseeds;	// seeds for RNGs of threads
	tseeds = (uint32_t*)malloc(numthreads * sizeof(uint32_t));
	for (int i = 0; i < numthreads; i++) {
		tseeds[i] = (uint32_t)mrand();	// seeds for RNG of each thread
	}

	if (rnn >= 1) { // choose random neearest neighbor initialization
		randnn(h_idx[0], d, tseeds, dist_tab, rnn);
	}
	for (int i = 0; i < numthreads; i++) { // indices should be 0 based
		for (int k = 0; k < d; k++) {
			h_idx[i][k] = h_idx[0][k];
			h_idx_out[i][k] = h_idx[0][k];
			idx_saved[i][k] = h_idx[0][k];
		}
	}

	// if an input index file is given, load random permuation from it
	if (pfname != NULL) {
		FILE *pfile_ptr;
		pfile_ptr	= fopen(pfname, "rb");
		if (pfile_ptr == NULL) {
			printf("Could not open output file %s.\n", pfname);
		}
		else {
			if (fread(h_idx[0], sizeof(int), d, pfile_ptr) != d) {
				printf("An error occurred reading feature indices.\n");
				return -1;
			}
			for (int i = 0; i < numthreads; i++) { // indices should be 0 based
				for (int k = 0; k < d; k++) {
					h_idx[i][k] = h_idx[0][k];
					h_idx_out[i][k] = h_idx[0][k];
					idx_saved[i][k] = h_idx[0][k];
				}
			}
		}
		fclose(pfile_ptr);
	}

	// initialize starting states for each thread
	if ((style == 2) || (style == 3)) {
			for (int i = 1; i < numthreads; i++) {
				if (rnn == 0) {	// random initialization
					randperm(h_idx[i], d, tseeds+i);
				}
				else { 					// randpm nearest neighbor initialization
					randnn(h_idx[i], d, tseeds+i, dist_tab, rnn);
				}
				for (int k = 0; k < d; k++) {
					h_idx_out[i][k] = h_idx[i][k];
					idx_saved[i][k] = h_idx[i][k];
				}
			}
	}

	double Ts = -1.0 / log(Ps);						// start temperature
	double Tf = -1/log(Pf);		 					// stop temperature
	double F = pow(Tf/Ts,1.0/((float)(cooling-1))); // temperature reduction factor
	double** ent;									// entropy of shuffled patches
	double** ent_saved;								// entropy of shuffled patches
	double* ent_ori;								// entropy of original patches
	int* pairs;										// all possible pairs of grid indices
	ent = (double **)malloc(numthreads * sizeof(double*));
	ent_saved = (double **)malloc(numthreads * sizeof(double*));// initialize to 0
	ent_ori = (double *)malloc(d * sizeof(double));	// initialize to 0
	pairs = (int *)malloc(2 * num_neighbors * sizeof(int));
	if ((ent == NULL) || (ent_ori == NULL) || (ent_saved == NULL) || (pairs == NULL)) {
		printf("Memory allocation for ent, ent_ori, or pairs failed\n");
		return -1;
	}
	for (int i = 0; i < numthreads; i++) {
		ent[i] = (double *)malloc(d * sizeof(double));
		ent_saved[i] = (double *)malloc(d * sizeof(double));
		if ((ent[i] == NULL) || (ent_saved[i] == NULL)) {
			printf("Memory allocation for ent[%d] or ent_saved[%d] failed\n", i, i);
		}
	}

	int* idx;
	idx = (int *)malloc(d * sizeof(int));
	for (int i = 0; i < d; i++) {
		idx[i] = i;
	}
	entropy(ent_ori, dist_tab, idx, dim, d, w, h);			// entropy of original
	entropy(ent[0], dist_tab, h_idx[0], dim, d, w, h);	// entropy of thread 1 input
	if ((style == 2) || (style == 3)) {	// initial entropy of each thread
		for (int i = 1; i < numthreads; i++) {
			entropy(ent[i], dist_tab, h_idx[i], dim, d, w, h);
		}
	}

	// initialize ent and ent_saved for all threads
	for (int i = 0; i < numthreads; i++) {
		int j = 0;		// initialize with ent[0]
		if ((style == 2) || (style == 3)) {	// initialize with ent[i]
			j = i;
		}
		for (int k = 0; k < d; k++) {
			ent[i][k] = ent[j][k];
			ent_saved[i][k] = ent[j][k];
		}
	}

	// all possibles swaps
	int64_t cnt = 0;
	for (int i = 0; i < d-1; i++) {
		for (int j = i+1; j < d; j++) {
			pairs[2*cnt] = i;
			pairs[2*cnt+1] = j;
			cnt++;
		}
	}

	int dwsize = 2 * wsize;			// double the wsize
	double** affected_ent;			// total entropy for each window in a batch
	affected_ent = (double **)malloc(numthreads * sizeof(double*));
	for (int i = 0; i < numthreads; i++) {
		affected_ent[i] = (double *)malloc(dwsize * sizeof(double));
	}

	double* dEave;				// average of all accepted change of cost
	dEave = (double *)calloc(numthreads, sizeof(double));
	int64_t* a_cnt; 			// number of accepted swaps
	a_cnt = (int64_t *)calloc(numthreads, sizeof(int64_t));
	int64_t samplecnt = 0;		// number of pairs checked
	double T = Ts;				// starting temperature
	double* ENT;				// average entropy of local minimums
	double* ENT_saved;			// current optimal objective function value
	ENT_saved = (double *)malloc(numthreads * sizeof(double));
	ENT = (double *)malloc(((resets + 1) * cooling+1) * sizeof(double)); // add error check
	ENT[0] = mean(ent[0], d);	// starting entropy value
	ENT_saved[0] = ENT[0];	// important!!!
	for (int i = 1; i < numthreads; i++) {	// important!!!
		if ((style == 2) || (style == 3)) {
			ENT_saved[i] = mean(ent[i],d);
		}
		else {
			ENT_saved[i] = ENT[0];
		}
	}
	double ENT_ori = mean(ent_ori, d);		// entropy of the original feature arrangement
	printf("ENT[0] = %.12E, ENT_ori = %.12E\n", ENT[0], ENT_ori);

	int** stateidx;
	stateidx = (int **)malloc(numthreads * sizeof(int*));	// index for state pairs
	for (int i = 0; i < numthreads; i++) {
		stateidx[i] = (int *)malloc(num_neighbors * sizeof(int));
		for (int k = 0; k < num_neighbors; k++) {
			stateidx[i][k] = k;
		}
		randperm(stateidx[i], (int) num_neighbors, tseeds+i);	// initialize the order of visiting neighbors
	}

	// save intermediate results
	FILE *mfile_ptr;
	if (mfname != NULL) {
		mfile_ptr = fopen(mfname,"w+");
		if (mfile_ptr == NULL) {
			printf("Could not open %s.\n", mfname);
		}
		else {
			fprintf(mfile_ptr, "Input file: %s\n", ifname);
			fprintf(mfile_ptr, "Output file: %s\n", ofname);
			fprintf(mfile_ptr, "Topology dimension: %d\n", dim);
			fprintf(mfile_ptr, "Cooling: %d\n", cooling);
			fprintf(mfile_ptr, "Repeat: %d\n", repeat);
			fprintf(mfile_ptr, "Resets: %d\n", resets);
			fprintf(mfile_ptr, "Decay: %.2f\n", decay);
			fprintf(mfile_ptr, "Random seed: %d\n", rseed);
			fprintf(mfile_ptr, "Initial probability: %.5E\n", Ps);
			fprintf(mfile_ptr, "Final probability: %.5E\n", Pf);
			fprintf(mfile_ptr, "Window size: %d\n", wsize);
			fprintf(mfile_ptr, "Number of threads: %d\n", numthreads);
			fprintf(mfile_ptr, "Style of multithreaded search: %d\n", style);
			fprintf(mfile_ptr, "Random k-NN initialization: %d\n", rnn);
			fprintf(mfile_ptr, "ENT[0] = %.12E, ENT_ori = %.12E\n", ENT[0], ENT_ori);
			fflush(mfile_ptr);
		}
	}
	// save indices from each cooling steps
	FILE *xfile_ptr;
	if (xfname != NULL) {
		xfile_ptr = fopen(xfname, "wb");
		if (xfile_ptr == NULL) {
			printf("Could not open %s\n",xfname);
		}
		else {
			fwrite(&cooling, sizeof(int), 1, xfile_ptr);
			fwrite(&resets, sizeof(int), 1, xfile_ptr);
			fwrite(&d, sizeof(int), 1, xfile_ptr);
		}
	}

	int coolingstep = 0;		// cooling iteration
	double savedent = 0;		// the new objective function value to be saved
	int** affected;				// indices of unique windows affected by a swap
	affected = (int **)malloc(numthreads * sizeof(int*));
	for (int i = 0; i < numthreads; i++) {
		affected[i] = (int *)calloc(dwsize, sizeof(int));
	}
	int num_affected;			// number of windows affected by a swap
	double delta;				// change of objective function value
	double p,P;					// random numbers between 0 and 1;
	double epsilon = EPS*2*n;	// tolerance on rounding errors
	int s, e;		// indices of features to be swapped

	// Start measuring wall time
	time_t begin;
	time_t end;
	time(&begin);
	// formulate sequential and independent swaps
	int64_t repeatbd = repeat * num_neighbors;	// bound on repeatcnt

	// setup arguments for threads
	optimizer_struct **args = malloc(numthreads * sizeof(optimizer_struct*));
	for (int i = 0; i < numthreads; i++) {
		args[i] = (optimizer_struct*)malloc(sizeof(optimizer_struct));
		args[i] -> dim = dim;
		args[i] -> d = d;
		args[i] -> w = w;
		args[i] -> h = h;
		args[i] -> repeatbd = repeatbd;
		args[i] -> num_neighbors = num_neighbors;
		args[i] -> epsilon = epsilon;
		args[i] -> pairs = pairs;
		args[i] -> dist_tab = dist_tab;
		args[i] -> T = &T;
		args[i] -> h_idx = h_idx[i];
		args[i] -> h_idx_out = h_idx_out[i];
		args[i] -> idx_saved = idx_saved[i];
		args[i] -> stateidx = stateidx[i];
		args[i] -> affected = affected[i];
		args[i] -> a_cnt = a_cnt+i;
		args[i] -> ent = ent[i];
		args[i] -> ent_saved = ent_saved[i];
		args[i] -> affected_ent = affected_ent[i];
		args[i] -> dEave = dEave+i;
		args[i] -> ENT_saved = ENT_saved+i;
		args[i] -> seed = tseeds+i;
	}
	int bestthread = 0;

	// thread pointers
	pthread_t* t_id;
	t_id = (pthread_t *)malloc(numthreads * sizeof(pthread_t));

	while (1) {
		//break;
		for (int i = 1; i < numthreads; i++) {
			pthread_create(t_id+i, NULL, optimize, args[i]);
			//optimize(args[i]);
		}
		optimize(args[0]);
		for (int i = 1; i < numthreads; i++) {
			pthread_join(t_id[i], NULL);
		}

		coolingstep++;

		bestthread = mindouble(ENT_saved, numthreads); // find the current best thread
		ENT[coolingstep] = ENT_saved[bestthread]; // save the new objective function value
		if (mfname != NULL) {
			fprintf(mfile_ptr, "Coolingstep %d/%d: Best thread = %d, Min Entropy = %.12E\n", coolingstep-1, cooling, bestthread, ENT[coolingstep]);
			fflush(mfile_ptr);
		}
		else {
			printf("Coolingstep %d/%d: Best thread = %d, Min Entropy = %.12E\n", coolingstep-1, cooling, bestthread, ENT[coolingstep]);
		}

		if (xfname != NULL) {
			fwrite(&T, sizeof(double), 1, xfile_ptr);
			fwrite(&ENT[coolingstep], sizeof(double), 1, xfile_ptr);
			fwrite(h_idx[bestthread], sizeof(int), d, xfile_ptr);
		}

		// decrease the temperature if each pair is visited 'repeat' times
		T = F * T;	// decrease temperature

		if ((coolingstep % cooling) == 0) { // restart a run
			Ts = Ts * decay;	// initial temperature decay at each restart
			T = Ts;				// reset temperature
			printf("Reset temperature. T = %.10f\n", T);
			for (int i = 0; i < numthreads; i++) {
				int j = i;		// restart from current thread's minimum
				if ((style == 1) || (style == 3)) {	// restarts from the minimum of all threads
					j = bestthread;
					ENT_saved[i] = ENT_saved[j];
				}
				dEave[i] = 0.0;	// reset average delta
				a_cnt[i] = 0;	// reset a_cnt
				for (int k = 0; k < d; k++) {	// reset state variables
					h_idx[i][k] = idx_saved[j][k];
					h_idx_out[i][k] = idx_saved[j][k];
					idx_saved[i][k] = idx_saved[j][k];
					ent[i][k] = ent_saved[j][k];
					ent_saved[i][k] = ent_saved[j][k];	// important!!!
				}
			}
		}
		if (coolingstep >= (resets + 1) * cooling) { // terminate the search
			break;
		}
	}
	time(&end);
	double elapsed = difftime(end, begin);
	printf("Time: %.0f seconds\n", elapsed);
	printf("ENT_saved = %f\n", ENT_saved[bestthread]);
	printf("ENT_ori = %f\n", ENT_ori);
	printf("ENT[0] = %f\n", ENT[0]);
	printf("Percentage of improvement = %f\n", 100*(ENT[0] - ENT_saved[bestthread])/ENT[0]);
	if (d < 100) {
		printf("idx_saved : ");
		for (int i = 0; i < d; i++) {
			printf("%d ", idx_saved[bestthread][i]);
		}
	}

	// save new topology
	fwrite(idx_saved[bestthread], sizeof(int), d, ofile_ptr);
	fclose(ofile_ptr);

	if (mfname != NULL) {
		fclose(mfile_ptr);
	}
	if (xfname != NULL) {
		fclose(xfile_ptr);
	}

	free(a_cnt);
	free(dEave);
	free(ENT_saved);
	free(dist_tab);
	free(ent_ori);
	free(pairs);
	free(ENT);
	free(t_id);
	free(tseeds);
	free(idx);
	for (int i = 0; i < numthreads; i++) {
		free(args[i]);
		free(h_idx[i]);
		free(h_idx_out[i]);
		free(idx_saved[i]);
		free(affected[i]);
		free(ent[i]);
		free(ent_saved[i]);
		free(affected_ent[i]);
		free(stateidx[i]);
	}
	free(args);
	free(h_idx);
	free(h_idx_out);
	free(idx_saved);
	free(affected);
	free(ent);
	free(ent_saved);
	free(affected_ent);
	free(stateidx);
	return 0;
}
