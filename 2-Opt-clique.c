
/********************************************************************************/
/*****************  A 2-opt Local Search for Symmetric TSP    *******************/
/********************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#define MAX_WSIZE 256
const double MAX_DB = 1.7e+308;		// maximum double
const double PRECISION = -1.1e-16;	// precision for delta_min

/********************************************************************************/
/***************************** Function Prototypes ******************************/
/********************************************************************************/
void localSearchAlgorithm(int nTours);
void exchange(int ncand, int startingNode, double startingCost);
static int readInstance(char* filename);
void fatal(char* s);
void stripExtension(char *fname);
static double aveDist(int a, int b, int reverse);

/* variables for time function */
time_t clk1, clk2;

/********************************************************************************/
/************************* General Global Variables *****************************/
/********************************************************************************/

/* node information specific to each individual tour in the population			*/
struct NODE {					/* attributes of a node within a tour			*/
	int before;					/* previous node in the tour					*/
	int after;					/* next node in the tour						*/
};

struct NODE *city;		/* best solution found in one run of local search		*/
struct NODE* cityBest;	/* best solution found over all runs of local search	*/

int* CITY, ** City;				/* local search starting tours (permutations)	*/

double bestCost;				/* cost of best solution found in one run		*/
double overallBestCost;			/* cost of best solution over all runs			*/
int N;							/* number of cities								*/
int wsize;					// window size
int num_pairs;			// number of city pairs in side a window
int window[MAX_WSIZE];			// window of cities
int route[2*MAX_WSIZE-2];			// all affected cities

FILE* fp_sol;	/* final solution produced by the algorithm (.sol text file)	*/
FILE* fp_idx;	/* final solution produced by the algorithm (.idx binary file)	*/
FILE* fp_idxall;/* all solutions produced by the algorithm (.idx binary file)	*/
FILE* fp_out;	/* results report produced by the algorithm(.tab tab delimited	*/
				/* text file													*/

double* DIST, ** Dist;			/* distance matrix (symmetric) 					*/

/* Feature indices, part of the input file format. The initial idx permutation  */
/* is discarded in this code as it is the first permutation in the .cyc input   */
/* file of starting solutions. However, idx is still used here to store the     */
/*  2-opt final permutation as a linear array.	                        		*/
int* idx;

void fatal(char* s) {
	fprintf(stderr, "%s\n", s);
	exit(1);
}

/* distance between two cities */
double dist (int i, int j) {
	int temp, k;

	if (i == j)
		return ((double)0.0);

	if (j < i) {
		temp =  i;
		i = j;
		j = temp;
	}
	k = j - i - 1;  // position of column index j in row i of the triangular matrix

	return (Dist[i][k]);
}

void stripExtension(char *fname) {
    char *end = fname + strlen(fname);

    while (end > fname && *end != '.') {
        --end;
    }

    if (end > fname) {
        *end = '\0';
    }
}

/* random integer in the interval [a, b] */
int rnd(int a, int b) {
	int r;
	r=(rand() % (b - a + 1)) + a;

	return(r);
}

double rnd_double() {
	return rand() / (double)RAND_MAX;
}


static int readInstance(char *ifname) {

	/* Input file format: 			*/
	int n;							// sample size, 1 int32  (need it to define epsilon in topCPUT: discarded in this code)
	int d;							// feature dimension, 1 int32	(i.e. size of the image permutation vector), N = d here
	// int w;							// width of 2d topology, 1 int32 (not needed in this code)
	// int h;							// height of 2d topology, 1 int32 (not needed in this code)
	// int* idx;						// Feature indices. Variable made global in this code for convenience.
	int64_t num_neighbors;			// number of feature pairs (no order), 1 int64  (i.e. number of elements in the entropy matrix)
	// double* dist_tab;				// entropy table, num_neighbors double. It's been renamed to DIST in this code, and is now a triangular matrix
										// indexed by Dist[i][j]
	int64_t i;

	FILE *ifile_ptr;

	printf("%s\n", ifname);

	ifile_ptr = fopen(ifname,"rb");
	if (ifile_ptr == NULL) fatal("Couldn't open instance file");

	/* assumes dim = 1  */
	if ((fread(&n, sizeof(int), 1, ifile_ptr) != 1) ||
		(fread(&d, sizeof(int), 1, ifile_ptr) != 1) ||
		(fread(&num_neighbors, sizeof(int64_t), 1, ifile_ptr) != 1)) {
		printf("An error occurred reading the dimension values.\n");
		return -1;
	}

	int64_t D = (int64_t) d;
	if(num_neighbors !=  D * (D-1) / 2) {
		printf("Feature dimension does not match the size of distance matrix. %lld != %lld \n", num_neighbors, D * (D-1) / 2);
		return -1;
	}

	idx = (int *)malloc(d * sizeof(int));
	if (idx == NULL) fatal("Memory allocation for idx failed");

	if (fread(idx, sizeof(int), d, ifile_ptr) != d) {
		printf("An error occurred reading feature indices of shuffled image.\n");
		return -1;
	}

	printf("Number of samples is                 %d\n", n);
	printf("Number of features (cities) is            %d\n", d);
	printf("Number of edges            %lld\n", num_neighbors);

	N = d;   // number of cities

	/* allocate memory for the entropy matrix */
	DIST = (double*) malloc (num_neighbors * sizeof(double));
	Dist = (double**) malloc ((N-1) * sizeof(double*));

	/* set up pointers for rows of the off-diagonal upper-triangular matrix, diagonal not included. */
	for (i = 0; i < (N - 1); i++) {
		Dist[i] = DIST + i * ((int64_t) N) - ((int64_t) i) * (i + 1)/2;
	}

	printf("Loading entropy distances ...... \n");
	if (fread(DIST, sizeof(double), num_neighbors, ifile_ptr) != num_neighbors) {
		printf("Error reading distance matrix.\n");
		return -1;
	}

	fclose(ifile_ptr);
	return 1;

}

int readPopulation (char* initFilename) {

	int nCities, nTours;
	int countCities;
	int cityId;
	int cityIndex;
	int t, tour;
	FILE* fp_init;

	printf("%s\n", initFilename);

	/* Read in starting solution(s) */
	fp_init = fopen(initFilename, "r");
	if (fp_init == NULL) fatal("Couldn't open initial solution file.\n");

	/* read header of the file containing the initial population */
	fscanf (fp_init, "%d%d", &nCities, &nTours);
	if (nCities != N) fatal("Invalid number of cities");

	/* create table to store permutations in the population	*/
	CITY = (int*) malloc (nTours * N * sizeof(int));
	City = (int**) malloc (nTours * sizeof(int*));
	for (t = 0; t < nTours; t++) City[t] = CITY + t * N;

	countCities = 0;
	cityIndex = 0;
	tour = 0;
	while ((fscanf(fp_init, "%d", &cityId)) != EOF) {	/* read all tours from the initial population file */
		City[tour][cityIndex] = cityId;
		if (++cityIndex == N) {
			cityIndex = 0;
			tour++;
		}
		countCities++;
	}

	if (tour != nTours) fatal("Invalid number of tours in the population dataset");
	if (countCities/nTours != nCities) fatal("Invalid number of cities in the population dataset");

	printf("Population size: %d tours of %d cities each\n", tour, countCities/tour);

	fclose(fp_init);

	return (nTours);
}

/*	reads a given permutation from the population into a doubly-linked list for local search */
double readStartingTour(int tour) {
	int current, first, before;
	int cityIndex;
	double cost;

	cityIndex = 0;
	current = City[tour][cityIndex];
	first = before = current;
	cost = 0.0;

	for (int i = 0; i < N; i++) {		// window average cost
		for (int j = 0; j < wsize; j++) {
			window[j] = City[tour][(i+j)%N];
		}
		double tmp = 0;
		for (int j = 0; j< wsize-1; j++) {
			for (int k = j+1; k < wsize; k++) {
				tmp += dist(window[j],window[k]);
			}
		}
		cost += tmp / ((double) num_pairs);
	}

	while (++cityIndex < N) {
		current = City[tour][cityIndex];
		city[before].after = current;
		city[current].before = before;
		//cost += dist(before, current);
		before = current;
	}
	//cost += dist(before, first);	// "before" is now the last city in the permutation
	city[before].after = first;
	city[first].before = before;
	bestCost = cost;

	return(cost);
}

/* used to save a tour for LS */
void writeTourAll (struct NODE *city) {
	int current, next;
	int j;

	current = 0;
	next = city[current].after;
//	fprintf(fp_sol, "%d\n", current);
	j = 0;
	idx[j] = current;
	while (next != 0) {
		current = next;
		next = city[current].after;
//		fprintf(fp_sol, "%d\n", current);
		idx[++j] = current;
	}

	fwrite(idx, sizeof(int), N, fp_idxall);  // save new topology
}

/* rewrite writeTour to automatically detected the leftmost index */
void writeTour (struct NODE *city) {
	int current, next, tmpcurrent;
	int j;
	double maxval = -1;
//	double minval = MAX_DB;
	int maxidx = 0;
//	int minidx = 0;
	int start;

	current = 0;
	for (int i = 0; i < N; i++) {		// window average cost
		window[0] = current;
		tmpcurrent = current;
		for (int j = 1; j < wsize; j++) {
			window[j] = city[tmpcurrent].after;
			tmpcurrent = window[j];
		}
		double tmp = 0;
		for (int j = 0; j< wsize-1; j++) {
			for (int k = j+1; k < wsize; k++) {
				tmp += dist(window[j],window[k]);
			}
		}
		if (maxval < tmp) {
			maxval = tmp;
			maxidx = current;
		}
/*		if (minval >= tmp) {
			minval = tmp;
			minidx = current;
		}*/
		current = window[1];
	}
	current = city[maxidx].after;
	//current = (minval == 0.0) ? city[minidx].after : city[maxidx].after; // where to break the circle determines on if there is background
	start = current;
	next = city[current].after;
	fprintf(fp_sol, "%d\n", current);
	j = 0;
	idx[j] = current;
	while (next != start) {
		current = next;
		next = city[current].after;
		fprintf(fp_sol, "%d\n", current);
		idx[++j] = current;
	}

	// add ent of the tour
	double cost = 0.0;
	double* ent;
	ent = (double *)malloc(N * sizeof(double));
	for (int i = 0; i < N; i++) {		// window average cost
		for (int j = 0; j < wsize; j++) {
			window[j] = idx[(i+j)%N];
		}
		double tmp = 0;
		for (int j = 0; j< wsize-1; j++) {
			for (int k = j+1; k < wsize; k++) {
				tmp += dist(window[j],window[k]);
			}
		}
		ent[i] = tmp / ((double) num_pairs);
		cost += ent[i];
	}

	fwrite(idx, sizeof(int), N, fp_idx);  // save new topology
	fwrite(ent, sizeof(double), N, fp_idx);  // save ent of the new topology
	printf("total cost = %.10lf\n", cost/N);
	free(ent);
}
////////////////////////////////////////////////////////////////////

/************************************************************************/
/*******   local search algorithm	    *********************************/
/************************************************************************/
void localSearchAlgorithm(int nTours) {

	int t, j;
	int ncand;
	double runTime, overallRunTime;
	int bestRun;
	double currentCost;				// cost of current (working) solution
	double initialTotalCost;		// cost (fitness) of the initial population
	double initialAverageCost;		// average cost (fitness) of the initial population
	double sumBestCost;				// sum of all local optima (fitness) cost
	double averageBestCost;			// average cost of the nTours local optima


	city = (struct NODE*) malloc(N * sizeof(struct NODE));
	cityBest = (struct NODE*) malloc(N * sizeof(struct NODE));

	/* perform 2-opt local search on each tour in the initial population	*/
	overallRunTime = 0.0;
	initialTotalCost = 0.0;
	sumBestCost = 0.0;
	time(&clk1);
	for (t = 0; t < nTours; t++) {
		currentCost = readStartingTour(t);		// may pass filename as argv[2]
		initialTotalCost += currentCost;

		printf("2-Opt starting tour: %d, fitness: %.10lf\n", t, currentCost/N);

		ncand = N + 1;  // use N + 1 to consider all possible "a-b" moves for a fixed "a"
		exchange(ncand, (int) 0, currentCost);  // 2-Opt procedure

		printf("2-Opt best tour: %d, fitness: %.10lf\n", t, bestCost/N);

		writeTourAll(city); // save current solution

		/* initializes or updates best tour found so far */
		if (t == 0 ||  bestCost < overallBestCost) {
			overallBestCost = bestCost;
			for (j = 0; j < N; j++) {
				cityBest[j].before = city[j].before;
				cityBest[j].after = city[j].after;
				bestRun = t;
			}
		}
		sumBestCost += bestCost;
	}

	time(&clk2);
	overallRunTime = difftime(clk2, clk1);

	printf("2-Opt CPU Time over %d runs (starting tours): %9.4lf seconds\n", nTours, overallRunTime);
	printf("2-Opt best run: %d, fitness: %.10lf\n", bestRun, overallBestCost/N);

	initialAverageCost = initialTotalCost / nTours;		// average cost (fitness) of the initial population  (starting solutions)
	averageBestCost = sumBestCost / nTours;				// average cost (fitness) of the best solutions (local optima) for all runs
	fprintf(fp_out, "LS\t%d\t%d\t%.2lf\t%d\t%d\t%.2lf\t%.2lf\t%9.4lf\n", N, nTours, initialAverageCost, nTours, nTours, averageBestCost, overallBestCost, overallRunTime);

	writeTour(cityBest);

	free(city);
	free(cityBest);
}


/*********************************************************************/
/***   2-Opt local search procedure	    ******************************/
/*********************************************************************/
void exchange(int ncand, int startingNode, double startingCost) {
/* startingNode is the node from which the 2-opt search starts.		 */
/* It can be randomly generated in [0, N-1] for multiple executions. */

	double delta_min, delta;
	double drop, add, dist_a_a2;
	int w,  w2, v, v2;
	int improving;
	int startNode;

	double s;
	double currentCost;

	int Np;
	int a, a2, b, b2;
	int a0;
	int k, temp_1, temp_2;

	int64_t MAXITE = N * ((int) sqrt(N)); // avoid a rare infinite loop due to float precision
	int64_t ite = 0;
	double numpairs = (double) (wsize * (wsize-1)/2);

	s = currentCost = startingCost;
	a = startingNode;
	do {
		startNode = a;	// the given starting node or the "a" updated in the last search (the updated rule is shown below)
		improving = 0;
		delta_min = currentCost;	// initialized at the cost of the entire tour--a high enough value
		ite++;
		do {
			a2 = city[a].after;
			a0 = city[a].before;
			if (wsize == 2) {
				dist_a_a2 = dist(a, a2);	// compute (or call function) only once as the value won't change until "a" is changed
			}
			else {
				dist_a_a2 = aveDist(a, a2, 0);
			}
			Np = 0;
			b = city[a2].after;
			w = a;
			do {	// evaluate all possible moves for current "a", i.e. all moves that link "a" to every possible "b"
				Np++;
				b2 = city[b].after;
				if (wsize == 2) {
					drop = dist_a_a2 + dist(b, b2);		// use pre-computed dist_a_a2 as it doesn't depend on "b"
					add = dist(a,b) + dist(a2, b2);
				}
				else {
					drop = dist_a_a2 + aveDist(b, b2, 0);
					add = aveDist(a, b, 1) + aveDist(a2, b2, 2);
				}
				delta = add - drop;
				// keep track of the move that gives the best improvement among all moves for the given "a"
				if (delta < delta_min) {
					delta_min = delta;   // current lowest delta: not necessary an improvement the first time it is updated (see initialization above)
					// nodes "a" and "b" fully define the a 2-Opt move (as the other nodes will be their successors in the tour)
					//w = a;
					v = b;
				}
				b = b2;	// next "b" is the successor of the current one in the tour
			// if not all (candidate) "b's" have been considered, evaluate 2-opt exchange with the new b
			} while (b != a0 && Np < ncand);
			a = a2;	// picks the next "a" (as the successor of the current one) in case the search is to be continued
		// breaks off the search as soon as at last one improvement is found in the neighborhood for a given "a"
		} while (s + delta_min >= currentCost && a != startNode);
		s += delta_min;  //  new solution value

		// if an improvement on the current tour has been found, update (best) solution; check delta_min to avoid a very rare infinite loop due to finite precison of double.
		if (s < currentCost && delta_min < numpairs*PRECISION && ite <= MAXITE) {
			currentCost = s;
			improving = 1;
			// reverse subpath (a2, ..., b)
			w2 = city[w].after;
			v2 = city[v].after;
			k = city[w2].after;
			while (k != v) {
				temp_1 = city[k].before;
				city[k].before = city[k].after;
				temp_2 = city[k].after;
				city[k].after = temp_1;
				k = temp_2;
			}
			city[w2].before = city[w2].after;
			city[v].after = city[v].before;
			city[v2].before = w2;
			city[w2].after = v2;
			city[v].before = w;
			city[w].after = v;
		}
		// a possible new search is set to begin with the successor of the "a"-node that originated the current
		// best improvement (see update of starting node above)
		a = city[w].after;
		bestCost = currentCost;
	// if no improvement has been found, stop the search: the current best solution is a 2-opt local optimum
	// as the complete 2-opt neighborhood (for all "a's") must have been evaluated
	} while(improving);
	printf("ite = %lld, MAXITE = %lld\n", ite, MAXITE);
}

double aveDist(int a, int b, int reverse) {
	double cost = 0.0;
	int direction;
	int tmp;

	route[wsize-2] = a;
	route[wsize-1] = b;

	switch(reverse) {
		case 0 : // a -> a2 or b -> b2
			for (int i = wsize - 3; i >= 0; i--) {
				route[i] = city[route[i+1]].before;
			}
			for (int i = wsize; i < 2 * wsize - 2; i++) {
				route[i] = city[route[i-1]].after;
			}
			break;
		case 1 : // a -> b
			direction = 1;
			for (int i = wsize - 3; i >= 0; i--) {
				tmp = (direction == 1) ? city[route[i+1]].before : city[route[i+1]].after;
				if (tmp == b) {
					route[i] = city[a].after;
					direction = 2;
				}
				else {
					route[i] = tmp;
				}
			}
			direction = 1;
			for (int i = wsize; i < 2 * wsize - 2; i++) {
				tmp = (direction == 1) ? city[route[i-1]].before : city[route[i-1]].after;
				if (tmp == a) {
					route[i] = city[b].after;
					direction = 2;
				}
				else {
					route[i] = tmp;
				}
			}
			break;
		default :	// a2 -> b2
			direction = 2;
			for (int i = wsize - 3; i >= 0; i--) {
				tmp = (direction == 1) ? city[route[i+1]].before : city[route[i+1]].after;
				if (tmp == b) {
					route[i] = city[a].before;
					direction = 1;
				}
				else {
					route[i] = tmp;
				}
			}
			direction = 2;
			for (int i = wsize; i < 2 * wsize - 2; i++) {
				tmp = (direction == 1) ? city[route[i-1]].before : city[route[i-1]].after;
				if (tmp == a) {
					route[i] = city[b].before;
					direction = 1;
				}
				else {
					route[i] = tmp;
				}
			}
	}
/*
	for (int i = 0; i < wsize - 1; i++) {
		for (int j = i; j < wsize + i - 1; j++) {
			for (int k = j + 1; k < wsize + i; k++) {
				cost += dist(route[j],route[k]);
			}
		}
	}
	cost /= ((double) num_pairs);*/

	int len = 2*wsize - 2;
	int scale = 1;
	for (int i = 0; i < len; i++) {
		int upperbound = ((i + wsize) > len) ? len : i + wsize;
		for (int j = i+1; j < upperbound; j++) {
			scale = (i+1 < len-j) ? i+1 : len-j;
			scale = scale < wsize - j + i ? scale : wsize - j + i;
			cost += ((double) scale) * dist(route[i],route[j]);
		}
	}
	cost /= ((double) num_pairs);

	return cost;
}

int main(int argc, char** argv) {

	char reportFile[200];		// filename for the summary report produced by the algorithm (.tab, tab delimited file)
	char solutionFile[200];		// filename for the final (best) solution found by the algorithm (.sol, text file)
	char permutationFile[200];	// filename for the final (best) solution found by the algorithm (.idx, binary file for matlab)
	char permutationallFile[200];// filename for all solution found by the algorithm (.idxall, binary file for matlab)
	char initFilename[200];		// filename of the initial population of starting solutions (.cyc, text file of permutations)
	char* problemName;			// filename of the problem instance (.dist file of a 1D image file)

	int nTours;					// number of starting tours
	int i;

	if (argc != 3) {
		printf("usage: <program name>  <problem name> <window size>\n");
		exit(0);
	}
	wsize = atoi(argv[2]);
	num_pairs = wsize * (wsize - 1) / 2;
	if ((wsize > MAX_WSIZE) || (wsize < 2)) {
		printf("window size must be less than %d and larger than 1 ", MAX_WSIZE);
	}
	else {
		printf("wsize = %d\n", wsize);

		problemName = argv[1];  // full name including extension
		readInstance(problemName);

		stripExtension (problemName);	// remove extension from filename

		sprintf(reportFile, "%s.tab", problemName);
		fp_out = fopen(reportFile, "w");
		if (fp_out == NULL) fatal("Couldn't open final report file.");

		sprintf(solutionFile, "%s.sol", problemName);
		fp_sol = fopen(solutionFile, "w");
		if (fp_sol == NULL) fatal("Couldn't open final solution file.");

		sprintf(permutationFile, "%s.idx", problemName);
		fp_idx = fopen(permutationFile, "wb");
		if (fp_idx == NULL) fatal("Couldn't open final idx file.");
		sprintf(permutationallFile, "%s.idxall", problemName);
		fp_idxall = fopen(permutationallFile, "wb");
		if (fp_idxall == NULL) fatal("Couldn't open final idxall file.");

		sprintf(initFilename, "%s.cyc", problemName);
		nTours = readPopulation(initFilename);

		printf("LS: 2-opt\n");
		fprintf(fp_out, "%s\tProblem Size\tPopulation Size\tInitial Average Fitness\tGenerations\tFailures\tFinal Average Fitness\tSolution Fitness\tTime\n", problemName);

		localSearchAlgorithm(nTours);

		printf("Execution complete: results stored in file %s.\n", reportFile);

		fclose(fp_out);
		fclose(fp_sol);
		fclose(fp_idx);
		fclose(fp_idxall);
	}
	free(CITY);
	free(City);
	free(idx);
	free(Dist);
	free(DIST);

	return 0;
}
